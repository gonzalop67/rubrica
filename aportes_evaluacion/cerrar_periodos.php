<link href="calendario/calendar-blue.css" rel="stylesheet" type="text/css" />
<div class="content-wrapper">
	<div id="titulo_pagina">
		<?php echo $_SESSION['titulo_pagina'] ?>
	</div>
	<div id="barra_principal">
		<table id="tabla_navegacion" style="border: none;" cellpadding="0" cellspacing="0">
			<tr>
				<td width="5%" class="fuente9" class="text-right"> Per&iacute;odo:&nbsp; </td>
				<td width="5%"> 
					<select id="cboPeriodosEvaluacion" class="fuente8">
						<option value="0"> Seleccione... </option>
					</select> </td>
				<td width="5%" class="fuente9" class="text-center"> &nbsp;Paralelo:&nbsp; </td>
				<td width="5%"> <select id="cboParalelo" class="fuente8">
						<option value="0"> Seleccione... </option>
					</select> </td>
				<td width="*">&nbsp;</td> <!-- Esto es para igualar las columnas -->
			</tr>
		</table>
	</div>
	<div id="pag_periodo_evaluacion">
		<!-- Aqui va la paginacion de los periodos de evaluacion encontrados -->
		<div class="header2"> LISTA DE APORTES DE EVALUACION EXISTENTES </div>
		<div id="tabla" class="table-responsive">
			<table class="table fuente8">
				<thead>
					<tr>
						<th width='10%'>
							<input id="check_all" type="checkbox" style="vertical-align: middle;">
							<button type="button" name="process_all" id="process_all" class="btn btn-success btn-xs">Procesar</button>
						</th>
						<th width='5%'>Nro.</th>
						<th width='5%'>Id.</th>
						<th width='10%'>Nombre</th>
						<th width='10%'>Fecha de Apertura</th>
						<th width='10%'>Fecha de Cierre</th>
						<th width='10%'>Estado</th>
						<th width='40%'>Acciones</th>
					</tr>
				</thead>
				<tbody>
					<!-- Aquí se va a pintar los registros de forma dinámica mediante AJAX -->
				</tbody>
			</table>
		</div>
		<div id="lista_aportes_evaluacion" style="text-align:center"> </div>
	</div>
	<div id="mensaje" class="mensaje">Debe seleccionar un Per&iacute;odo de Evaluaci&oacute;n...</div>
</div>
<script type="text/javascript" src="js/funciones.js"></script>
<script type="text/JavaScript" language="javascript" src="calendario/calendar.js"></script>
<script type="text/JavaScript" language="javascript" src="calendario/lang/calendar-sp.js"></script>
<script type="text/JavaScript" language="javascript" src="calendario/calendar-setup.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		cargarPeriodosEvaluacion();
		cargarParalelos();
		//If check_all checked then check all table rows
		$("#check_all").on("click", function() {
			if ($("input:checkbox").prop("checked")) {
				$("input:checkbox[name='row-check']").prop("checked", true);
			} else {
				$("input:checkbox[name='row-check']").prop("checked", false);
			}
		});
		// Check each table row checkbox
		$("input:checkbox[name='row-check']").on("change", function() {
			var total_check_boxes = $("input:checkbox[name='row-check']").length;
			var total_checked_boxes = $("input:checkbox[name='row-check']:checked").length;

			// If all checked manually then check check_all checkbox
			if (total_check_boxes === total_checked_boxes) {
				$("#check_all").prop("checked", true);
			} else {
				$("#check_all").prop("checked", false);
			}
		});
		$("#cboPeriodosEvaluacion").change(function(e) {
			e.preventDefault();
			$("#tabla tbody").html("");
			$('#cboParalelo').val(0);
			$("#mensaje").html("Debe seleccionar un Paralelo...");
			$("#cboParalelo").focus();
		});
		$("#cboParalelo").change(function(e) {
			e.preventDefault();
			var id_paralelo = $("#cboParalelo").val();
			if (id_paralelo == 0) {
				$("#tabla tbody").html("");
				$("#mensaje").html("Debe seleccionar un Paralelo...");
			} else {
				// Aquí toca cargar los cierres definidos para el Paralelo y el periodo elegidos
				cargarAportesEvaluacion(false);
			}
		});
	});

	function sel_texto(input) {
		$(input).select();
	}

	function cargarPeriodosEvaluacion() {
		$.get("scripts/cargar_periodos_evaluacion.php", {},
			function(resultado) {
				if (resultado == false) {
					alert("Error");
				} else {
					$("#cboPeriodosEvaluacion").append(resultado);
				}
			}
		);
	}

	function cargarParalelos() {
		$.get("scripts/cargar_paralelos_especialidad.php", function(resultado) {
			if (resultado == false) {
				alert("Error");
			} else {
				$('#cboParalelo').append(resultado);
			}
		});
	}

	function cargarAportesEvaluacion(iDesplegar) {
		var id_periodo_evaluacion = $("#cboPeriodosEvaluacion").val();
		var id_paralelo = $("#cboParalelo").val();
		if (id_periodo_evaluacion == 0) {
			$("#mensaje").css("color", "red");
			$("#mensaje").html("Debe seleccionar un Per&iacute;odo de Evaluaci&oacute;n...");
			$("#lista_aportes_evaluacion").html("");
			$("#cboPeriodosEvaluacion").focus();
		} else if (id_paralelo == 0) {
			$("#mensaje").css("color", "red");
			$("#mensaje").html("Debe seleccionar un Paralelo...");
			$("#lista_aportes_evaluacion").html("");
			$("#cboParalelo").focus();
		} else {
			$.ajax({
				type: "POST",
				url: "scripts/listar_aportes_estados.php",
				data: "id_periodo_evaluacion=" + id_periodo_evaluacion + "&id_paralelo=" + id_paralelo,
				success: function(resultado) {
					if (!iDesplegar) $("#mensaje").html("");
					//$("#lista_aportes_evaluacion").html(resultado);
					$("#tabla tbody").html(resultado);
				}
			});
		}
	}

	function actualizarAporteEvaluacion(id, nombre, fecha_apertura, fecha_cierre) {
		var txt_fecha_apertura = fecha_apertura.value;
		var txt_fecha_cierre = fecha_cierre.value;
		var id_paralelo = $("#cboParalelo").val();
		$("#mensaje").html("<img src='imagenes/ajax-loader.gif' alt='procesando...' />");
		$.ajax({
			type: "POST",
			url: "scripts/actualizar_fechas_aporte_evaluacion.php",
			data: "id_aporte_evaluacion=" + id + "&id_paralelo=" + id_paralelo + "&ap_nombre=" + nombre + "&fecha_apertura=" + txt_fecha_apertura + "&fecha_cierre=" + txt_fecha_cierre,
			success: function(resultado) {
				$("#mensaje").css("color", "blue");
				$("#mensaje").html(resultado);
				cargarAportesEvaluacion(true);
			}
		});
	}
</script>