<link href="calendario/calendar-blue.css" rel="stylesheet" type="text/css" />
<div class="content-wrapper">
    <div id="cerrarPeriodosEvaluacion" class="col-sm-12">
        <br>
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h4 class="text-center">Abrir / Cerrar Periodos de Evaluación</h4>
            </div>
            <div class="panel-body">
                <form id="form_malla" action="" class="app-form">
                    <div class="row">
                        <div class="col-sm-2 text-right">
                            <label class="control-label" style="position:relative; top:7px;">Periodo:</label>
                        </div>
                        <div class="col-sm-10">
                            <select class="form-control fuente9" id="cboPeriodosEvaluacion">
                                <option value="0">Seleccione...</option>
                            </select>
                            <span class="help-desk error" id="mensaje1"></span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-2 text-right">
                            <label class="control-label" style="position:relative; top:7px;">Paralelo:</label>
                        </div>
                        <div class="col-sm-10" style="margin-top: 2px;">
                            <select class="form-control fuente9" id="cboParalelo">
                                <option value="0">Seleccione...</option>
                            </select>
                            <span class="help-desk error" id="mensaje2"></span>
                        </div>
                    </div>
                    <div class="row" id="botones_insercion">
                        <div class="col-sm-12" style="margin-top: 4px;">
                            <button id="btn-add-item" type="button" class="btn btn-block btn-primary" onclick="procesarFechas()" disabled>
                                Procesar
                            </button>
                        </div>
                    </div>
                </form>
                <!-- Línea de división -->
                <hr>
                <!-- message -->
                <div id="text_message" class="fuente9 text-center"></div>
                <!-- table -->
                <table id="tabla" class="table fuente9">
                    <thead>
                        <tr>
                            <th>Nro.</th>
                            <th>Id.</th>
                            <th>Nombre</th>
                            <th>Fecha de Apertura</th>
                            <th>Fecha de Cierre</th>
                            <th>Estado</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody id="lista_items">
                        <!-- Aqui desplegamos el contenido de la base de datos -->
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="js/funciones.js"></script>
<script type="text/JavaScript" language="javascript" src="calendario/calendar.js"></script>
<script type="text/JavaScript" language="javascript" src="calendario/lang/calendar-sp.js"></script>
<script type="text/JavaScript" language="javascript" src="calendario/calendar-setup.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        cargarPeriodosEvaluacion();
        cargarParalelos();
        //If check_all checked then check all table rows
        $("#check_all").on("click", function() {
            if ($("input:checkbox").prop("checked")) {
                $("input:checkbox[name='row-check']").prop("checked", true);
            } else {
                $("input:checkbox[name='row-check']").prop("checked", false);
            }
        });
        // Check each table row checkbox
        $("input:checkbox[name='row-check']").on("change", function() {
            var total_check_boxes = $("input:checkbox[name='row-check']").length;
            var total_checked_boxes = $("input:checkbox[name='row-check']:checked").length;

            // If all checked manually then check check_all checkbox
            if (total_check_boxes === total_checked_boxes) {
                $("#check_all").prop("checked", true);
            } else {
                $("#check_all").prop("checked", false);
            }
        });
        $("#cboPeriodosEvaluacion").change(function(e) {
            e.preventDefault();
            $("#tabla tbody").html("");
            $('#cboParalelo').val(0);
            $("#mensaje").html("Debe seleccionar un Paralelo...");
            $("#cboParalelo").focus();
        });
        $("#cboParalelo").change(function(e) {
            e.preventDefault();
            var id_paralelo = $("#cboParalelo").val();
            if (id_paralelo == 0) {
                $("#tabla tbody").html("");
                $("#mensaje").html("Debe seleccionar un Paralelo...");
            } else {
                // Aquí toca cargar los cierres definidos para el Paralelo y el periodo elegidos
                cargarAportesEvaluacion(false);
            }
        });
    });

    function sel_texto(input) {
        $(input).select();
    }

    function cargarPeriodosEvaluacion() {
        $.get("scripts/cargar_periodos_evaluacion.php", {},
            function(resultado) {
                if (resultado == false) {
                    alert("Error");
                } else {
                    $("#cboPeriodosEvaluacion").append(resultado);
                }
            }
        );
    }

    function cargarParalelos() {
        $.get("scripts/cargar_paralelos_especialidad.php", function(resultado) {
            if (resultado == false) {
                alert("Error");
            } else {
                $('#cboParalelo').append(resultado);
            }
        });
    }

    function cargarAportesEvaluacion(iDesplegar) {
        var id_periodo_evaluacion = $("#cboPeriodosEvaluacion").val();
        var id_paralelo = $("#cboParalelo").val();
        if (id_periodo_evaluacion == 0) {
            $("#mensaje").css("color", "red");
            $("#mensaje").html("Debe seleccionar un Per&iacute;odo de Evaluaci&oacute;n...");
            $("#lista_aportes_evaluacion").html("");
            $("#cboPeriodosEvaluacion").focus();
        } else if (id_paralelo == 0) {
            $("#mensaje").css("color", "red");
            $("#mensaje").html("Debe seleccionar un Paralelo...");
            $("#lista_aportes_evaluacion").html("");
            $("#cboParalelo").focus();

        } else {
            $.ajax({
                type: "POST",
                url: "scripts/listar_aportes_estados.php",
                data: "id_periodo_evaluacion=" + id_periodo_evaluacion + "&id_paralelo=" + id_paralelo,
                success: function(resultado) {
                    if (!iDesplegar) $("#mensaje").html("");
                    //$("#lista_aportes_evaluacion").html(resultado);
                    $("#tabla tbody").html(resultado);
                }
            });
        }
    }

    function actualizarAporteEvaluacion(id, nombre, fecha_apertura, fecha_cierre) {
        var txt_fecha_apertura = fecha_apertura.value;
        var txt_fecha_cierre = fecha_cierre.value;
        var id_paralelo = $("#cboParalelo").val();
        $("#mensaje").html("<img src='imagenes/ajax-loader.gif' alt='procesando...' />");
        $.ajax({
            type: "POST",
            url: "scripts/actualizar_fechas_aporte_evaluacion.php",
            data: "id_aporte_evaluacion=" + id + "&id_paralelo=" + id_paralelo + "&ap_nombre=" + nombre + "&fecha_apertura=" + txt_fecha_apertura + "&fecha_cierre=" + txt_fecha_cierre,
            success: function(resultado) {
                $("#mensaje").css("color", "blue");
                $("#mensaje").html(resultado);
                cargarAportesEvaluacion(true);
            }
        });
    }
</script>