<?php
require('../fpdf16/fpdf.php');
require('../scripts/clases/class.mysql.php');
require('../scripts/clases/class.paralelos.php');
require('../scripts/clases/class.institucion.php');
require('../scripts/clases/class.tipos_educacion.php');
require('../scripts/clases/class.periodos_lectivos.php');

function equiv_rendimiento($id_periodo_lectivo, $calificacion)
{
	$db = new MySQL();
	// Determinacion de la letra de equivalencia que corresponde a la calificacion dada
	$escala_calificacion = $db->consulta("SELECT * FROM sw_escala_calificaciones WHERE id_periodo_lectivo = $id_periodo_lectivo");
	$equivalencia = "";
	while ($escala = $db->fetch_assoc($escala_calificacion)) {
		$nota_minima = $escala["ec_nota_minima"];
		$nota_maxima = $escala["ec_nota_maxima"];
		if ($calificacion >= $nota_minima && $calificacion <= $nota_maxima) {
			$equivalencia = $escala["ec_equivalencia"];
			break;
		}
	}
	return $equivalencia;
}

function truncar($numero, $digitos)
{
	$truncar = pow(10, $digitos);
	return intval($numero * $truncar) / $truncar;
}

class PDF extends FPDF
{
	var $nombreParalelo = "";
	var $nombreInstitucion = "";
	var $nomNivelEducacion = "";
	var $logoInstitucion = "";
	var $nombrePeriodoLectivo = "";
	var $nombrePeriodoEvaluacion = "";
	var $id_periodo_evaluacion = "";
	var $regimen = "";

	//Cabecera de página
	function Header()
	{
		//Logo Izquierda
		$this->Image('ministerio.png', 10, 8, 33);
		//Logo Derecha
		$logoInstitucion = dirname(dirname(__FILE__)) . '/public/uploads/' . $this->logoInstitucion;
		$this->Image($logoInstitucion, 247, 5, 23);

		$this->SetFont('Arial', 'B', 13);
		$title1 = $this->nombreInstitucion;
		$w = $this->GetStringWidth($title1);
		$this->SetX((297 - $w) / 2);
		$this->Cell($w, 10, $title1, 0, 0, 'C');
		$this->Ln(4);

		$this->SetFont('Arial', '', 9);
		$title2 = $this->nomNivelEducacion;
		$w = $this->GetStringWidth($title2);
		$this->SetX((297 - $w) / 2);
		$this->Cell($w, 10, $title2, 0, 0, 'C');

		$this->Ln(4);
		$this->SetFont('Arial', 'B', 7);
		$this->Cell(195, 10, "REGIMEN: ", 0, 0, 'R');
		$this->SetFont('Arial', '', 7);
		$this->Cell(10, 10, $this->regimen, 0, 0, 'R');

		$this->Ln(3);
		$this->SetFont('Arial', '', 11);
		$title3 = "REPORTE DE " . $this->nombrePeriodoEvaluacion;
		$w = $this->GetStringWidth($title3);
		$this->SetX((297 - $w) / 2);
		$this->Cell($w, 10, $title3, 0, 0, 'C');

		$this->Ln(3);
		$this->SetFont('Arial', 'B', 7);
		$title4 = utf8_decode("AÑO LECTIVO: " . $this->nombrePeriodoLectivo);
		$w = $this->GetStringWidth($title4);
		$this->SetX((297 - $w) / 2);
		$this->Cell($w, 10, $title4, 0, 0, 'C');
		$this->Line(10, 30, 297 - 10, 30); // 20mm from each edge
		$this->Ln();

		$this->SetFont('Arial', 'B', 8);
		$asignatura = "ASIGNATURA: " . $this->nombreAsignatura;
		$w = $this->GetStringWidth($asignatura);
		$this->Cell($w, 8, $asignatura, 0, 0, 'L');

		// Aqui va el codigo para imprimir el nombre del curso y paralelo
		$w = $this->GetStringWidth($this->nombreParalelo);
		$this->SetX(287 - $w);
		$this->Cell($w, 8, $this->nombreParalelo, 0, 0, 'R');
		// $this->Ln();

		$this->Cell(92, 6, " ", 0, 0, 'C');
		$db = new MySQL();
		// Aqui calculamos el ancho para las etiquetas de los parciales
		// Obtener el número de aportes de evaluación
		$aportes = $db->consulta("SELECT ap_abreviatura,
								  		 id_tipo_aporte, 
								  		 ap_ponderacion 
							 		FROM sw_aporte_evaluacion 
								   WHERE id_periodo_evaluacion = $this->id_periodo_evaluacion
								   ORDER BY ap_orden");
		// while ($aporte = $db->fetch_assoc($aportes)) {
		// 	$id_aporte_evaluacion = $aporte["id_aporte_evaluacion"];
		// 	$abreviatura = $aporte["ap_abreviatura"];
		// 	// Calculamos el ancho para la etiqueta del parcial
		// 	$rubricas = $db->consulta("SELECT id_rubrica_evaluacion 
		// 								 FROM sw_rubrica_evaluacion
		// 								WHERE id_aporte_evaluacion = $id_aporte_evaluacion");
		// 	$total_rubricas = $db->num_rows($rubricas);
		// 	$width = 12 * ($total_rubricas + 1);
		// 	$this->Cell($width, 6, $abreviatura, 1, 0, 'C');
		// }
		while ($titulo_aporte = $db->fetch_assoc($aportes)) {
			// $cadena .= "<th>" . $titulo_aporte["ap_abreviatura"] . "</th>\n";
			// $cadena .= "<th>" . ($titulo_aporte["ap_ponderacion"] * 100) . "%</th>";
			
		}
		$this->Ln();
		$this->SetFont('Arial', 'B', 8);
		$this->Cell(8, 6, 'Nro.', 1, 0, 'C');
		$this->Cell(84, 6, utf8_decode('Nómina'), 1, 0, 'C');
		// Aqui imprimimos las etiquetas de las rubricas de cada aporte
		$aportes = $db->consulta("SELECT ap_abreviatura, 
										 id_tipo_aporte, 
										 ap_ponderacion 
									FROM sw_aporte_evaluacion 
	  							   WHERE id_tipo_aporte = 1  
	  								 AND id_periodo_evaluacion = $this->id_periodo_evaluacion");
		$suma_ponderacion = 0;
		while ($aporte = $db->fetch_assoc($aportes)) {
			$suma_ponderacion += $aporte["ap_ponderacion"];
			$this->Cell(12, 6, $aporte["ap_abreviatura"], 1, 0, 'C');
		}
		$this->Cell(12, 6, "PROM", 1, 0, 'C');
		$this->Cell(12, 6, ($suma_ponderacion * 100) . "%", 1, 0, 'C');
		// Aquí comprueba si existe examen de subperiodo
		$consulta = $db->consulta("SELECT ap_abreviatura, 
									ap_ponderacion 
									FROM sw_aporte_evaluacion 
									WHERE id_periodo_evaluacion = $this->id_periodo_evaluacion
									AND id_tipo_aporte = 2");
		$num_total_registros = $db->num_rows($consulta);
		if ($num_total_registros > 0) {
			$registro = $db->fetch_assoc($consulta);
			$this->Cell(12, 6, $registro["ap_abreviatura"], 1, 0, 'C');
			$this->Cell(12, 6, ($registro["ap_ponderacion"] * 100) . "%", 1, 0, 'C');
		}
		$this->SetFillColor(204, 204, 204);
		$periodo = $db->consulta("SELECT pe_abreviatura
									 FROM sw_periodo_evaluacion
									WHERE id_periodo_evaluacion = $this->id_periodo_evaluacion");
		$registro = $db->fetch_assoc($periodo);
		$this->Cell(12, 6, $registro["pe_abreviatura"], 1, 0, 'C', 1);
		$this->Cell(12, 6, "EQUI.", 1, 0, 'C', 1);
		$this->Ln();
	}
}

// Variables enviadas mediante POST
$id_paralelo = $_POST["id_paralelo"];
$id_asignatura = $_POST["id_asignatura"];
$id_periodo_evaluacion = $_POST["id_periodo_evaluacion"];

session_start();
$id_periodo_lectivo = $_SESSION["id_periodo_lectivo"];
$id_usuario = $_SESSION["id_usuario"];

$db = new MySQL();

// Obtengo el tutor del grado/curso
$consulta = $db->consulta("SELECT us_shortname FROM sw_usuario u, sw_paralelo_tutor p WHERE u.id_usuario = p.id_usuario AND p.id_paralelo = $id_paralelo AND p.id_periodo_lectivo = $id_periodo_lectivo");
$resultado = $db->fetch_assoc($consulta);
$nombreTutor = utf8_decode($resultado["us_shortname"]);

// Obtengo el nombre del docente
$consulta = $db->consulta("SELECT us_shortname FROM sw_usuario WHERE id_usuario = $id_usuario");
$resultado = $db->fetch_assoc($consulta);
$nombreDocente = utf8_decode($resultado["us_shortname"]);

$paralelo = new paralelos();
$nombreParalelo = utf8_decode($paralelo->obtenerNombreParalelo($id_paralelo));

$periodo_lectivo = new periodos_lectivos();
$nombrePeriodoLectivo = $periodo_lectivo->obtenerNombrePeriodoLectivo($id_periodo_lectivo);

// Obtener el nombre del periodo de evaluación
$consulta = $db->consulta("SELECT pe_nombre FROM sw_periodo_evaluacion WHERE id_periodo_evaluacion = $id_periodo_evaluacion");
$resultado = $db->fetch_object($consulta);
$nombrePeriodoEvaluacion = $resultado->pe_nombre;

$institucion = new institucion();
$nombreInstitucion = utf8_decode($institucion->obtenerNombreInstitucion());
$logoInstitucion = $institucion->obtenerLogoInstitucion();
$nombreRector = utf8_decode($institucion->obtenerNombreRector());
$regimen = utf8_decode($institucion->obtenerRegimenInstitucion());

//Obtengo el Nivel de Educación
$nivelEducacion = new tipos_educacion();
$nomNivelEducacion = utf8_decode($nivelEducacion->obtenerNombreTipoEducacion($id_paralelo));

//Creación del objeto de la clase heredada
$pdf = new PDF('L');
$pdf->SetTopMargin(4);

$pdf->nomNivelEducacion = $nomNivelEducacion;
$pdf->nombreParalelo = $nombreParalelo;
$pdf->nombrePeriodoLectivo = $nombrePeriodoLectivo;
$pdf->nombreInstitucion = $nombreInstitucion;
$pdf->logoInstitucion = $logoInstitucion;
$pdf->nombreRector = $nombreRector;
$pdf->nombreParalelo = $nombreParalelo;
$pdf->id_periodo_evaluacion = $id_periodo_evaluacion;
$pdf->nombrePeriodoEvaluacion = $nombrePeriodoEvaluacion;
$pdf->regimen = $regimen;

$fecha_actual = date_create(date('Y-m-d')); //fecha actual

// Obtengo el tutor del grado/curso
$consulta = $db->consulta("SELECT us_shortname FROM sw_usuario u, sw_paralelo_tutor p WHERE u.id_usuario = p.id_usuario AND p.id_paralelo = $id_paralelo AND p.id_periodo_lectivo = $id_periodo_lectivo");
$resultado = $db->fetch_assoc($consulta);
$nombreTutor = utf8_decode($resultado["us_shortname"]);

// Obtengo quien inserta el comportamiento (0 : Docentes, 1 : Tutor)
$consulta = $db->consulta("SELECT quien_inserta_comp FROM sw_curso c, sw_paralelo p WHERE c.id_curso = p.id_curso AND p.id_paralelo = $id_paralelo");
$resultado = $db->fetch_assoc($consulta);
$quien_inserta_comportamiento = $resultado["quien_inserta_comp"];

// Aqui va el codigo para imprimir el nombre de la asignatura
$consulta = $db->consulta("SELECT as_nombre FROM sw_asignatura WHERE id_asignatura = $id_asignatura");
$resultado = $db->fetch_assoc($consulta);
$pdf->nombreAsignatura = utf8_decode($resultado["as_nombre"]);

// Aqui va el codigo para imprimir las calificaciones de los estudiantes
$consulta = $db->consulta("SELECT e.id_estudiante, 
								  di.id_asignatura, 
								  e.es_apellidos, 
								  e.es_nombres, 
								  id_tipo_asignatura 
							 FROM sw_distributivo di, 
								  sw_estudiante_periodo_lectivo ep, 
								  sw_estudiante e, 
								  sw_asignatura a,
								  sw_paralelo p
							WHERE di.id_paralelo = ep.id_paralelo 
							  AND di.id_periodo_lectivo = ep.id_periodo_lectivo 
							  AND ep.id_estudiante = e.id_estudiante 
							  AND di.id_asignatura = a.id_asignatura 
							  AND di.id_paralelo = p.id_paralelo 
							  AND di.id_paralelo = $id_paralelo
	                          AND di.id_asignatura = $id_asignatura
	                          AND es_retirado <> 'S'
							  AND activo = 1 
							ORDER BY es_apellidos, es_nombres ASC");
$num_total_registros = $db->num_rows($consulta);
if ($num_total_registros > 0) {
	$contador = 0;
	$pdf->AddPage();
	while ($paralelo = $db->fetch_assoc($consulta)) {
		$contador++;

		$pdf->SetFont('Arial', '', 8);
		$pdf->Cell(8, 6, $contador, 1, 0, 'C');
		$nombre_completo = utf8_decode($paralelo["es_apellidos"]) . " " . utf8_decode($paralelo["es_nombres"]);
		$pdf->Cell(84, 6, $nombre_completo, 1, 0, 'L');
		//Aqui van las calificaciones de cada estudiante...
		$id_estudiante = $paralelo["id_estudiante"];
		$id_tipo_asignatura = $paralelo["id_tipo_asignatura"];

		// Aqui se calculan los promedios de cada aporte de evaluacion
		$aportes = $db->consulta("SELECT a.id_aporte_evaluacion, 
										 ap_ponderacion 
									FROM sw_periodo_evaluacion p, 
										 sw_aporte_evaluacion a 
								   WHERE p.id_periodo_evaluacion = a.id_periodo_evaluacion 
									 AND p.id_periodo_evaluacion = $id_periodo_evaluacion
								     AND ap_tipo = 1");
		$num_total_registros = $db->num_rows($aportes);
		/* if ($num_total_registros > 0) {
			// Aqui calculo los promedios y desplegar en la tabla
			$suma_aportes = 0;
			$contador_aportes = 0;
			$suma_promedios = 0;
			while ($aporte = $db->fetch_assoc($aportes)) {
				$contador_aportes++;
				$rubrica_evaluacion = $db->consulta("SELECT id_rubrica_evaluacion 
				  									   FROM sw_rubrica_evaluacion r,
					   										sw_asignatura a
				 									  WHERE r.id_tipo_asignatura = a.id_tipo_asignatura
				   										AND a.id_asignatura = $id_asignatura
				   										AND id_aporte_evaluacion = " . $aporte["id_aporte_evaluacion"]);
				$total_rubricas = $db->num_rows($rubrica_evaluacion);
				if ($total_rubricas > 0) {
					$suma_rubricas = 0;
					$contador_rubricas = 0;
					while ($rubricas = $db->fetch_assoc($rubrica_evaluacion)) {
						$contador_rubricas++;
						$id_rubrica_evaluacion = $rubricas["id_rubrica_evaluacion"];
						$qry = $db->consulta("SELECT re_calificacion FROM sw_rubrica_estudiante WHERE id_estudiante = $id_estudiante AND id_paralelo = " . $id_paralelo . " AND id_asignatura = " . $id_asignatura . " AND id_rubrica_personalizada = " . $id_rubrica_evaluacion);
						$total_registros = $db->num_rows($qry);
						if ($total_registros > 0) {
							$rubrica_estudiante = $db->fetch_assoc($qry);
							$calificacion = $rubrica_estudiante["re_calificacion"];
						} else {
							$calificacion = 0;
						}
						$suma_rubricas += $calificacion;
						$pdf->Cell(12, 6, $calificacion == 0 ? " " : $calificacion, 1, 0, 'C');
					}
				}
				$promedio = $suma_rubricas / $contador_rubricas;
				$suma_promedios += $promedio;
				$pdf->Cell(12, 6, $promedio == 0 ? " " : substr($promedio, 0, strpos($promedio, '.') + 3), 1, 0, 'C');
			}
			// Aqui debo calcular el ponderado de los promedios parciales
			$promedio_aportes = $suma_promedios / $contador_aportes;
			$pdf->Cell(12, 6, $promedio_aportes == 0 ? " " : substr($promedio_aportes, 0, strpos($promedio_aportes, '.') + 3), 1, 0, 'C');
			$ponderado_aportes = 0.8 * $promedio_aportes;
			$pdf->Cell(12, 6, $ponderado_aportes == 0 ? " " : substr($ponderado_aportes, 0, strpos($ponderado_aportes, '.') + 3), 1, 0, 'C');
			$aportes = $db->consulta("SELECT re_calificacion 
										FROM sw_rubrica_estudiante re, 
										     sw_rubrica_evaluacion r, 
											 sw_periodo_evaluacion p, 
											 sw_aporte_evaluacion a
								   	   WHERE r.id_rubrica_evaluacion = re.id_rubrica_personalizada 
										 AND r.id_aporte_evaluacion = a.id_aporte_evaluacion 
										 AND p.id_periodo_evaluacion = a.id_periodo_evaluacion 
									 	 AND p.id_periodo_evaluacion = $id_periodo_evaluacion 
										 AND id_estudiante = $id_estudiante 
										 AND id_paralelo = $id_paralelo 
										 AND id_asignatura = $id_asignatura 
								     	 AND ap_tipo = 2");
			$num_total_registros = $db->num_rows($aportes);
			if ($num_total_registros > 0) {
				$record = $db->fetch_assoc($aportes);
				$examen_quimestral = $record["re_calificacion"];
			} else {
				$examen_quimestral = 0;
			}
			$pdf->Cell(12, 6, $examen_quimestral == 0 ? " " : $examen_quimestral, 1, 0, 'C');
			$ponderado_examen = 0.2 * $examen_quimestral;
			$pdf->Cell(12, 6, $ponderado_examen == 0 ? " " : substr($ponderado_examen, 0, strpos($ponderado_examen, '.') + 3), 1, 0, 'C');
			$calificacion_quimestral = $ponderado_aportes + $ponderado_examen;
			$pdf->SetFillColor(204, 204, 204);
			$pdf->Cell(12, 6, $calificacion_quimestral == 0 ? " " : substr($calificacion_quimestral, 0, strpos($calificacion_quimestral, '.') + 3), 1, 0, 'C', 1);
			$pdf->Cell(12, 6, $calificacion_quimestral == 0 ? " " : equiv_rendimiento($id_periodo_lectivo, $calificacion_quimestral), 1, 0, 'C', 1);
		} */

		if ($num_total_registros > 0) {
			// Aqui calculo los promedios y desplegar en la tabla
			$suma_aportes = 0;
			$contador_aportes = 0;
			$suma_promedios = 0;
			$suma_ponderados = 0;
			while ($aporte = $db->fetch_assoc($aportes)) {
				$contador_aportes++;
				$tipo_aporte = $aporte["id_tipo_aporte"];
				$estado_aporte = $aporte["ap_estado"];
				$ponderacion = $aporte["ap_ponderacion"];
				$rubrica_evaluacion = $db->consulta("SELECT id_rubrica_evaluacion 
													   FROM sw_rubrica_evaluacion r,
															sw_asignatura a
													  WHERE r.id_tipo_asignatura = a.id_tipo_asignatura
														AND a.id_asignatura = $id_asignatura
														AND id_aporte_evaluacion = " . $aporte["id_aporte_evaluacion"]);
				$total_rubricas = $db->num_rows($rubrica_evaluacion);
				if ($total_rubricas > 0) {
					$suma_rubricas = 0;
					$contador_rubricas = 0;
					while ($rubricas = $db->fetch_assoc($rubrica_evaluacion)) {
						$contador_rubricas++;
						$id_rubrica_evaluacion = $rubricas["id_rubrica_evaluacion"];
						$qry = $db->consulta("SELECT re_calificacion FROM sw_rubrica_estudiante WHERE id_estudiante = " . $paralelos["id_estudiante"] . " AND id_paralelo = " . $id_paralelo . " AND id_asignatura = " . $id_asignatura . " AND id_rubrica_personalizada = " . $id_rubrica_evaluacion);
						$total_registros = $db->num_rows($qry);
						if ($total_registros > 0) {
							$rubrica_estudiante = $db->fetch_assoc($qry);
							$calificacion = $rubrica_estudiante["re_calificacion"];
						} else {
							$calificacion = 0;
						}
						$suma_rubricas += $calificacion;
					}
				}
				$promedio = $suma_rubricas / $contador_rubricas;
				$promedio_ponderado = $promedio * $ponderacion;
				$promedio = $promedio == 0 ? "" : substr($promedio, 0, strpos($promedio, '.') + 3);
				$cadena .= "<td>" . $promedio . "</td>";

				$pdf->Cell(12, 6, $promedio == 0 ? " " : substr($promedio, 0, strpos($promedio, '.') + 3), 1, 0, 'C');
				
				$suma_promedios += $promedio;
				$suma_ponderados += $promedio_ponderado;
			}
			
			// Aqui debo calcular el ponderado de los promedios parciales
			$promedio_aportes = $suma_promedios / $contador_aportes;
			// $ponderado_aportes = 0.8 * $promedio_aportes;
			$ponderado_aportes = $suma_ponderados;

			$promedio_aportes = $promedio_aportes == 0 ? "" : substr($promedio_aportes, 0, strpos($promedio_aportes, '.') + 3);
			$cadena .= "<td>" . $promedio_aportes . "</td>";

			$ponderado_aportes = $ponderado_aportes == 0 ? "" : substr($ponderado_aportes, 0, strpos($ponderado_aportes, '.') + 4);
			$cadena .= "<td>" . $ponderado_aportes . "</td>";

			// Obtener la calificación del examen de subperiodo si existe

			$aportes = $db->consulta("SELECT id_aporte_evaluacion, 
			                                 ap_ponderacion 
										FROM sw_aporte_evaluacion 
									   WHERE id_periodo_evaluacion = $id_periodo_evaluacion 
									     AND id_tipo_aporte = 2");
			$num_total_registros = $db->num_rows($aportes);

			if ($num_total_registros > 0) {
				$aporte = $db->fetch_assoc($aportes);
				$ponderacion = $aporte["ap_ponderacion"];
				$rubrica_evaluacion = $db->consulta("SELECT id_rubrica_evaluacion 
													   FROM sw_rubrica_evaluacion r,
															sw_asignatura a
													  WHERE r.id_tipo_asignatura = a.id_tipo_asignatura
														AND a.id_asignatura = $id_asignatura
														AND id_aporte_evaluacion = " . $aporte["id_aporte_evaluacion"]);
				$total_rubricas = $db->num_rows($rubrica_evaluacion);
				if ($total_rubricas > 0) {
					$rubrica = $db->fetch_assoc($rubrica_evaluacion);
					$id_rubrica_evaluacion = $rubrica["id_rubrica_evaluacion"];
					$qry = $db->consulta("SELECT re_calificacion FROM sw_rubrica_estudiante WHERE id_estudiante = " . $paralelos["id_estudiante"] . " AND id_paralelo = " . $id_paralelo . " AND id_asignatura = " . $id_asignatura . " AND id_rubrica_personalizada = " . $id_rubrica_evaluacion);
					$total_registros = $db->num_rows($qry);
					if ($total_registros > 0) {
						$rubrica_estudiante = $db->fetch_assoc($qry);
						$examen_quimestral = $rubrica_estudiante["re_calificacion"];
					} else {
						$examen_quimestral = 0;
					}
					// Desplegar la calificacion del examen de subperiodo
					$examen_quimestral = ($examen_quimestral == 0) ? "" : number_format($examen_quimestral, 2);
					$cadena .= "<td>" . $examen_quimestral . "</td>";
					$ponderado_examen = $ponderacion * $examen_quimestral;
					$ponderado_examen = $ponderado_examen == 0 ? "" : substr($ponderado_examen, 0, strpos($ponderado_examen, '.') + 4);
					$cadena .= "<td>" . $ponderado_examen . "</td>";
					$calificacion_quimestral = $ponderado_aportes + $ponderado_examen;
				}
			} else {
				$calificacion_quimestral = $ponderado_aportes;
			}

			$calificacion_quimestral = $calificacion_quimestral == 0 ? "" : substr($calificacion_quimestral, 0, strpos($calificacion_quimestral, '.') + 3);
			$cadena .= "<td>" . $calificacion_quimestral . "</td>";
		}

		$pdf->Ln();

		if ($contador % 21 == 0) $pdf->AddPage();
	}
} else {
	$pdf->Cell(100, 10, "No se han matriculado estudiantes en este paralelo...", 0, 0, 'L');
}
$pdf->Ln();
$pdf->SetFont('Arial', '', 8);
//Aqui van las firmas de docente y tutor
$pdf->Cell(0, 10, '___________________________', 0, 0, 'L');
$titulo1 = '___________________________';
$w = $pdf->GetStringWidth($titulo1);
$pdf->SetX(200 - $w);
$pdf->Cell($w, 8, $titulo1, 0, 0, 'R');
$pdf->Ln(5);
$pdf->Cell(0, 10, '      ' . $nombreDocente, 0, 0, 'L');
$titulo2 = '            ' . $nombreTutor;
$w = $pdf->GetStringWidth($titulo2);
$pdf->SetX(190 - $w);
$pdf->Cell($w, 8, $titulo2, 0, 0, 'R');
$pdf->Ln(5);
$pdf->SetFont('Arial', 'B', 8);
$pdf->Cell(0, 10, '               DOCENTE', 0, 0, 'L');
$titulo3 = '            TUTOR(A)';
$w = $pdf->GetStringWidth($titulo3);
$pdf->SetX(185 - $w);
$pdf->Cell($w, 8, $titulo3, 0, 0, 'R');

$pdf->Output();
