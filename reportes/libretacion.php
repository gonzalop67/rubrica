<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>R&uacute;brica Web 2.0</title>
	<style>
		#tabla_navegacion {
			border: none;
		}
	</style>
	<script type="text/javascript" src="js/funciones.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			cargarParalelos();

			$("#cboParalelos").change(function(e) {
				e.preventDefault();
				document.getElementById("id_paralelo").value = $(this).val();
				if ($(this).val() == 0) {
					$("#lista_estudiantes").addClass("text-danger");
					$("#lista_estudiantes").html("Debe seleccionar un paralelo...");
				} else {
					contarEstudiantesParalelo($(this).val()); //Esta funcion desencadena las demas funciones de paginacion
				}
			});
		});

		function cargarParalelos() {
			$.get("scripts/cargar_paralelos.php", {},
				function(resultado) {
					if (resultado == false) {
						alert("Error");
					} else {
						$("#cboParalelos").append(resultado);
					}
				}
			);
		}

		function contarEstudiantesParalelo(id_paralelo) {
			var numero_pagina = $("#numero_pagina").val();
			$.post("calificaciones/contar_estudiantes_paralelo.php", {
					id_paralelo: id_paralelo
				},
				function(resultado) {
					if (resultado == false) {
						alert("Error");
					} else {
						var JSONNumRegistros = eval('(' + resultado + ')');
						var total_registros = JSONNumRegistros.num_registros;
						$("#num_estudiantes").html("N&uacute;mero de Estudiantes encontrados: " + total_registros);
						if (total_registros == 0) {
							$("#paginacion_estudiantes").html("");
							$("#lista_estudiantes").html("No existen estudiantes matriculados en este paralelo...");
						} else {
							listarEstudiantesParalelo(id_paralelo);
						}
					}
				}
			);
		}

		function listarEstudiantesParalelo(id_paralelo) {
			var id_paralelo = document.getElementById("cboParalelos").value;
			$("#lista_estudiantes").html('<img src="imagenes/ajax-loader.gif" alt="procesando..." />');
			$.post("reportes/listar_estudiantes_libretacion.php", {
					id_paralelo: id_paralelo
				},
				function(resultado) {
					$("#lista_estudiantes").removeClass("text-danger");
					$("#lista_estudiantes").html(resultado);
				}
			);
		}
	</script>
</head>

<body>
	<div id="pagina">
		<div id="titulo_pagina">
			<?php echo $_SESSION['titulo_pagina'] ?>
		</div>
		<div id="barra_principal">
			<table id="tabla_navegacion" cellpadding="0" cellspacing="0">
				<tr>
					<td width="5%" class="text-right fuente9">Per&iacute;odo:&nbsp; </td>
					<td width="5%" class="text-right fuente9">Paralelo:&nbsp;</td>
					<td width="5%">
						<select id="cboParalelos" class="fuente8">
							<option value="0"> Seleccione... </option>
						</select>
					</td>

					<td width="85%">
						<form id="formulario_libretas" action="reportes/reporte_libreta.php" target="_blank" method="post">
							<div id="lista_estudiantes_paralelo" style="text-align:center">
								<!-- Aquí va el listado de estudiantes generado mediante AJAX -->
							</div>
							<input id="id_paralelo" name="id_paralelo" type="hidden" />
							<input id="id_estudiante" name="id_estudiante" type="hidden" />
						</form>
					</td>
				</tr>
			</table>
		</div>
		<div id="pag_nomina_estudiantes">
			<!-- Aqui va la paginacion de los estudiantes encontrados -->
			<div id="total_registros_estudiantes" class="paginacion">
				<table class="fuente8" width="100%" cellspacing=4 cellpadding=0 border=0>
					<tr>
						<td>
							<div id="num_estudiantes">&nbsp;N&uacute;mero de Estudiantes encontrados:&nbsp;</div>
						</td>
						<td>
							<div id="paginacion_estudiantes">
								<!-- Aqui va la paginacion de estudiantes -->
							</div>
						</td>
					</tr>
				</table>
			</div>
			<div id="pag_estudiantes">
				<!-- Aqui va la paginacion de los estudiantes encontrados -->
				<div class="header2"> LISTA DE ESTUDIANTES MATRICULADOS </div>

				<div id="tabla" class="table-responsive">
					<div id="lista_estudiantes" class="text-center text-danger">Debe seleccionar un paralelo...</div>
				</div>
			</div>
		</div>
	</div>
</body>

</html>