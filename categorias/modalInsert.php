<!-- Nueva Categoria Modal -->
<div class="modal fade" id="nuevaCategoriaModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title text-center" id="exampleModalLabel">Nueva Categoria</h4>
            </div>
            <form id="form_insert" onsubmit="return insertarCategoria()" autocomplete="off">
                <div class="modal-body fuente10">
                    <div class="form-group row">
                        <label for="category" class="col-sm-2 col-form-label">Nombre:</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" style="text-transform: uppercase;" id="category" name="category" value="" required>
                            <span id="mensaje1" style="color: #e73d4a"></span>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="exam_time_in_minutes" class="col-sm-2 col-form-label">Tiempo en minutos:</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="exam_time_in_minutes" name="exam_time_in_minutes" value="" required>
                            <span id="mensaje2" style="color: #e73d4a"></span>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Cancelar</button>
                    <button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-save"></span> Guardar</a>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- Fin Nueva Modalidad Modal -->