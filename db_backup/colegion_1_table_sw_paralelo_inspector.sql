
-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_paralelo_inspector`
--

DROP TABLE IF EXISTS `sw_paralelo_inspector`;
CREATE TABLE `sw_paralelo_inspector` (
  `id_paralelo_inspector` int(11) NOT NULL,
  `id_paralelo` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `id_periodo_lectivo` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Volcado de datos para la tabla `sw_paralelo_inspector`
--

INSERT INTO `sw_paralelo_inspector` (`id_paralelo_inspector`, `id_paralelo`, `id_usuario`, `id_periodo_lectivo`) VALUES
(97, 71, 13, 5),
(96, 83, 36, 5),
(107, 79, 36, 5),
(92, 68, 40, 5),
(18, 18, 27, 2),
(19, 19, 27, 2),
(20, 20, 27, 2),
(21, 21, 27, 2),
(22, 24, 27, 2),
(23, 25, 27, 2),
(24, 30, 27, 2),
(25, 23, 27, 2),
(26, 28, 27, 2),
(27, 29, 27, 2),
(36, 22, 27, 2),
(37, 26, 27, 2),
(38, 27, 27, 2),
(39, 23, 28, 2),
(40, 28, 28, 2),
(41, 29, 28, 2),
(91, 67, 40, 5),
(93, 69, 40, 5),
(110, 77, 36, 5),
(106, 84, 36, 5),
(100, 75, 13, 5),
(108, 73, 36, 5),
(101, 82, 13, 5),
(95, 70, 36, 5),
(109, 78, 13, 5),
(99, 81, 13, 5),
(98, 72, 13, 5),
(102, 76, 13, 5),
(94, 80, 40, 5),
(111, 86, 40, 6),
(112, 87, 40, 6),
(113, 88, 40, 6),
(114, 89, 40, 6),
(115, 98, 40, 6),
(116, 100, 40, 6),
(117, 103, 40, 6),
(118, 101, 40, 6),
(119, 102, 40, 6),
(120, 90, 40, 6),
(121, 91, 40, 6),
(122, 93, 40, 6),
(123, 104, 40, 6),
(124, 94, 40, 6),
(125, 95, 40, 6),
(126, 97, 40, 6),
(127, 105, 40, 6),
(128, 106, 40, 7),
(129, 107, 40, 7),
(130, 108, 40, 7),
(131, 109, 40, 7),
(132, 110, 40, 7),
(133, 111, 40, 7),
(134, 112, 40, 7),
(135, 113, 40, 7),
(136, 114, 40, 7),
(137, 115, 40, 7),
(138, 116, 40, 7),
(139, 117, 40, 7),
(146, 123, 40, 7),
(141, 118, 40, 7),
(142, 119, 40, 7),
(143, 120, 40, 7),
(144, 121, 40, 7),
(145, 122, 40, 7),
(147, 124, 40, 8),
(148, 125, 40, 8),
(149, 126, 40, 8),
(150, 129, 40, 8),
(151, 130, 40, 8),
(152, 131, 40, 8),
(153, 132, 40, 8),
(154, 133, 40, 8),
(198, 159, 68, 9),
(156, 135, 40, 8),
(157, 136, 40, 8),
(158, 137, 40, 8),
(159, 138, 40, 8),
(160, 139, 40, 8),
(161, 140, 40, 8),
(162, 142, 40, 8),
(163, 141, 40, 8),
(201, 164, 68, 12),
(197, 156, 68, 9),
(196, 158, 68, 9),
(195, 155, 68, 9),
(194, 154, 68, 9),
(193, 153, 68, 9),
(192, 152, 68, 9),
(191, 151, 68, 9),
(190, 150, 68, 9),
(189, 149, 68, 9),
(188, 148, 68, 9),
(200, 163, 68, 12),
(187, 147, 68, 9),
(186, 146, 68, 9),
(185, 145, 68, 9),
(184, 143, 68, 9),
(199, 160, 68, 10),
(182, 161, 40, 11),
(183, 162, 40, 11),
(202, 165, 68, 13),
(203, 166, 68, 13),
(204, 167, 68, 13),
(205, 168, 68, 13),
(206, 169, 68, 13),
(207, 170, 68, 13),
(208, 171, 68, 14),
(209, 172, 68, 14),
(210, 173, 68, 14),
(211, 174, 68, 14),
(212, 186, 598, 16),
(213, 182, 598, 16),
(214, 183, 598, 16),
(215, 185, 598, 16),
(216, 187, 598, 16),
(217, 195, 68, 19),
(218, 196, 68, 19),
(219, 197, 68, 19),
(220, 198, 68, 19),
(221, 199, 68, 19),
(222, 203, 68, 19),
(223, 201, 68, 19),
(224, 202, 68, 19),
(225, 209, 68, 21),
(226, 210, 68, 21),
(227, 211, 68, 21),
(228, 214, 68, 21),
(229, 215, 68, 21),
(230, 212, 68, 21),
(231, 213, 68, 21),
(232, 206, 68, 20),
(233, 207, 68, 20),
(234, 216, 68, 24),
(235, 217, 68, 24),
(236, 218, 68, 24);
