
-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_asignatura`
--

DROP TABLE IF EXISTS `sw_asignatura`;
CREATE TABLE `sw_asignatura` (
  `id_asignatura` int(11) NOT NULL,
  `id_area` int(11) NOT NULL,
  `id_tipo_asignatura` int(11) NOT NULL,
  `as_nombre` varchar(84) NOT NULL,
  `as_abreviatura` varchar(12) NOT NULL,
  `as_shortname` varchar(45) NOT NULL,
  `as_curricular` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Volcado de datos para la tabla `sw_asignatura`
--

INSERT INTO `sw_asignatura` (`id_asignatura`, `id_area`, `id_tipo_asignatura`, `as_nombre`, `as_abreviatura`, `as_shortname`, `as_curricular`) VALUES
(1, 2, 1, 'MATEMATICA', 'MAT', 'MATEMÁTICA', 1),
(2, 3, 1, 'ESTUDIOS SOCIALES', 'EESS', 'ESTUDIOS SOCIALES', 1),
(3, 4, 1, 'CIENCIAS NATURALES', 'CCNN', 'CIENCIAS NATURALES', 1),
(4, 6, 1, 'EDUCACION FISICA', 'EDU.F.', '2', 1),
(5, 11, 1, 'DIBUJO TECNICO APLICADO', 'DIB', '0', 1),
(6, 1, 1, 'LENGUA Y LITERATURA', 'LENGUA', 'LENGUA Y LITERATURA', 1),
(7, 7, 1, 'INGLES', 'ING', 'INGLÉS', 1),
(8, 4, 1, 'BIOLOGIA', 'BIO', 'BIOLOGÍA', 1),
(9, 4, 1, 'FISICA', 'FIS', 'FÍSICA', 1),
(10, 4, 1, 'QUIMICA', 'QUIM', 'QUÍMICA', 1),
(11, 9, 1, 'INFORMATICA APLICADA A LA EDUCACION', 'INFO', '0', 1),
(12, 5, 1, 'EDUCACION CULTURAL Y ARTISTICA', 'ED.ART', 'EDUCACIÓN ARTÍSTICA', 1),
(13, 1, 1, 'LENGUA Y LITERATURA', 'LENGUA', 'LENGUA Y LITERATURA', 1),
(14, 11, 1, 'FUNDAMENTOS DE PROGRAMACION', 'FUND.P', 'PROG. Y BBDD', 1),
(15, 11, 1, 'SISTEMAS INFORMATICOS MONOUSUARIOS Y MULTIUSUARIOS', 'MONO', '0', 1),
(16, 4, 1, 'FISICO QUIMICA', 'FISQ', 'QUÍMICA', 1),
(17, 6, 1, 'EDUCACION FISICA', 'EDU.F.', '0', 1),
(18, 5, 1, 'EDUCACION ESTETICA', 'EDU.E.', 'EDUCACIÓN ARTÍSTICA', 1),
(19, 3, 1, 'HISTORIA', 'HIST', 'HISTORIA', 1),
(20, 3, 1, 'EDUCACION PARA LA CIUDADANIA', 'EDU.C.', 'EDUCACIÓN/CIUDADANIA', 1),
(21, 3, 1, 'DESARROLLO DEL PENSAMIENTO FILOSOFICO', 'DES.P.', 'FILOSOFÍA', 1),
(22, 11, 1, 'GESTOR DE BASES DE DATOS', 'GBDD', '0', 1),
(23, 11, 1, 'REDES DE AREA LOCAL', 'REDES', '0', 1),
(24, 11, 1, 'IMPLANTACION DE APLICACIONES INFORMATICAS DE GESTION', 'MIAIG', '0', 1),
(25, 9, 1, 'EMPRENDIMIENTO Y GESTION', 'EMPRE', 'EMPRENDIMIENTO Y GESTIÓN', 1),
(26, 10, 1, 'GESTION ADMINISTRATIVA DE COMPRA Y VENTA', 'COMPRA', '0', 1),
(27, 9, 1, 'OPTATIVA', 'OPTA', '2', 1),
(28, 10, 1, 'CONTABILIDAD GENERAL Y TESORERIA', 'CONTA', 'CONTABILIDAD GENERAL', 1),
(29, 10, 1, 'GESTION ADMINISTRATIVA DE LOS RECURSOS HUMANOS', 'RRHH', '0', 1),
(30, 10, 1, 'PRODUCTOS Y SERVICIOS FINANCIEROS Y DE SEGUROS BASICOS', 'PROD', '0', 1),
(31, 10, 1, 'APLICACIONES INFORMATICAS', 'APL.I.', '0', 1),
(32, 10, 1, 'COMUNICACION, ARCHIVO DE LA INFORMACION Y OPERATORIA DE TECLADOS', 'COMU', '0', 1),
(33, 11, 1, 'RELACIONES EN EL ENTORNO DE TRABAJO', 'RET', '0', 1),
(34, 11, 1, 'FORMACION Y ORIENTACION LABORAL', 'FOL', 'FOL', 1),
(35, 11, 1, 'DESARROLLO DE FUNCIONES EN EL SISTEMA INFORMATICO', 'DES.F.', '0', 1),
(40, 11, 1, 'FORMACION EN CENTROS DE TRABAJO', 'FCT', 'FCT', 1),
(152, 10, 1, 'CONTABILIDAD GENERAL Y TESORERIA', 'CONTA', 'CONTABILIDAD GENERAL', 1),
(153, 3, 1, 'FILOSOFIA', 'FILO', 'FILOSOFÍA', 1),
(157, 12, 1, 'APLICACIONES OFIMATICAS LOCALES Y EN LINEA', 'APL.OF.', 'APLICACIONES OFIMÁTICAS', 1),
(158, 12, 1, 'SISTEMAS OPERATIVOS Y REDES', 'SIS.OP.', 'S. OPERATIVOS Y REDES', 1),
(159, 12, 1, 'PROGRAMACION Y BASES DE DATOS', 'PROG.', 'PROGRAMACIÓN Y BASE DE DATOS', 1),
(160, 12, 1, 'SOPORTE TECNICO', 'SOP.TEC.', 'SOPORTE TÉCNICO', 1),
(161, 12, 1, 'DISEÑO Y DESARROLLO WEB', 'DIS.WEB', 'DISEÑO WEB', 1),
(162, 10, 1, 'CONTABILIDAD DE COSTOS', 'COSTOS', 'CONTABILIDAD DE COSTOS', 1),
(163, 10, 1, 'CONTABILIDAD BANCARIA', 'CON.BAN.', 'CONTABILIDAD BANCARIA', 1),
(164, 10, 1, 'TRIBUTACION', 'TRIB.', 'TRIBUTACION', 1),
(165, 10, 1, 'GESTION DEL TALENTO HUMANO', 'G.TAL.H.', 'G. TALENTO HUMANO', 1),
(166, 10, 1, 'PAQUETES CONTABLES Y TRIBUTARIOS', 'PAQ.CON.', 'PAQUETES CONTABLES', 1),
(167, 10, 1, 'CONTABILIDAD GENERAL', 'CONTA.', 'CONTABILIDAD GENERAL', 1),
(168, 10, 1, 'FORMACION Y ORIENTACION LABORAL', 'FOL', 'FOL', 1),
(169, 12, 1, 'FORMACION Y ORIENTACION LABORAL', 'FOL', 'FOL', 1),
(170, 10, 2, 'FORMACION EN CENTROS DE TRABAJO', 'FCT', 'FCT', 0),
(171, 12, 2, 'FORMACION EN CENTROS DE TRABAJO', 'FCT', 'FCT', 0),
(172, 9, 2, 'DESARROLLO HUMANO INTEGRAL', 'DHI', 'DESARROLLO HUMANO', 1);
