
DROP TABLE IF EXISTS `sw_paralelo_tutor`;
CREATE TABLE `sw_paralelo_tutor` (
  `id_paralelo_tutor` int(11) NOT NULL,
  `id_paralelo` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `id_periodo_lectivo` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

INSERT INTO `sw_paralelo_tutor` (`id_paralelo_tutor`, `id_paralelo`, `id_usuario`, `id_periodo_lectivo`) VALUES
(1, 17, 44, 2),
(2, 18, 46, 2),
(3, 19, 52, 2),
(4, 20, 50, 2),
(5, 21, 48, 2),
(6, 22, 55, 2),
(8, 24, 49, 2),
(9, 25, 57, 2),
(10, 26, 56, 2),
(12, 28, 53, 2),
(13, 29, 45, 2),
(14, 30, 54, 2),
(15, 23, 59, 2),
(16, 27, 47, 2),
(17, 31, 64, 3),
(33, 32, 69, 3),
(19, 33, 50, 3),
(20, 34, 52, 3),
(21, 35, 57, 3),
(22, 36, 65, 3),
(23, 46, 53, 3),
(24, 39, 66, 3),
(25, 40, 46, 3),
(26, 43, 54, 3),
(27, 45, 56, 3),
(28, 44, 47, 3),
(29, 37, 48, 3),
(30, 41, 67, 3),
(31, 38, 59, 3),
(32, 42, 45, 3),
(34, 49, 64, 4),
(51, 51, 54, 4),
(58, 53, 79, 4),
(37, 56, 69, 4),
(38, 55, 72, 4),
(39, 50, 47, 4),
(49, 47, 74, 4),
(41, 48, 65, 4),
(42, 64, 59, 4),
(56, 59, 48, 4),
(44, 58, 45, 4),
(55, 57, 49, 4),
(46, 60, 53, 4),
(57, 55, 66, 4),
(50, 61, 55, 4),
(53, 54, 57, 4),
(54, 63, 67, 4),
(96, 78, 19, 5),
(97, 84, 68, 5),
(92, 75, 6, 5),
(91, 73, 15, 5),
(82, 67, 2, 5),
(83, 68, 73, 5),
(84, 69, 21, 5),
(85, 80, 40, 5),
(95, 77, 3, 5),
(87, 83, 20, 5),
(93, 82, 10, 5),
(94, 76, 587, 5),
(86, 70, 7, 5),
(88, 71, 26, 5),
(98, 79, 24, 5),
(89, 72, 71, 5),
(90, 81, 43, 5),
(99, 86, 19, 6),
(100, 87, 21, 6),
(101, 88, 73, 6),
(102, 89, 26, 6),
(103, 90, 24, 6),
(104, 94, 68, 6),
(105, 98, 3, 6),
(106, 100, 20, 6),
(107, 103, 71, 6),
(114, 91, 40, 6),
(109, 95, 587, 6),
(111, 93, 15, 6),
(112, 104, 10, 6),
(113, 97, 6, 6),
(115, 101, 13, 6),
(116, 102, 2, 6),
(118, 105, 594, 6);
