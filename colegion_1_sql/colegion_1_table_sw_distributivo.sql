
DROP TABLE IF EXISTS `sw_distributivo`;
CREATE TABLE `sw_distributivo` (
  `id_distributivo` int(11) NOT NULL,
  `id_periodo_lectivo` int(11) NOT NULL,
  `id_malla_curricular` int(11) NOT NULL,
  `id_paralelo` int(11) NOT NULL,
  `id_asignatura` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `sw_distributivo` (`id_distributivo`, `id_periodo_lectivo`, `id_malla_curricular`, `id_paralelo`, `id_asignatura`, `id_usuario`) VALUES
(1, 5, 3, 67, 6, 13),
(3, 5, 9, 68, 6, 13),
(4, 5, 49, 73, 22, 6),
(5, 5, 148, 76, 35, 6),
(6, 5, 104, 75, 14, 6),
(7, 5, 116, 82, 14, 6),
(8, 5, 149, 76, 23, 6),
(9, 5, 135, 79, 31, 6),
(10, 5, 150, 76, 33, 6),
(11, 5, 151, 76, 34, 6),
(12, 5, 55, 70, 8, 68),
(13, 5, 64, 83, 8, 68),
(14, 5, 121, 71, 10, 68),
(15, 5, 156, 72, 10, 68),
(16, 5, 163, 81, 10, 68),
(17, 5, 96, 75, 10, 68),
(18, 5, 108, 82, 10, 68),
(19, 5, 142, 76, 10, 68),
(20, 5, 72, 78, 10, 68),
(21, 5, 84, 84, 10, 68),
(22, 5, 130, 79, 10, 68),
(23, 5, 36, 77, 32, 36),
(24, 5, 37, 77, 152, 36),
(25, 5, 80, 78, 32, 36),
(26, 5, 92, 84, 32, 36),
(27, 5, 137, 79, 30, 36),
(28, 5, 23, 80, 2, 19),
(29, 5, 123, 71, 19, 19),
(30, 5, 97, 75, 19, 19),
(31, 5, 109, 82, 19, 19),
(32, 5, 73, 78, 19, 19),
(33, 5, 85, 84, 19, 19),
(34, 5, 133, 79, 19, 19),
(35, 5, 143, 76, 19, 19),
(36, 5, 45, 73, 20, 19),
(37, 5, 33, 77, 20, 19),
(38, 5, 98, 75, 20, 19),
(39, 5, 110, 82, 20, 19),
(40, 5, 74, 78, 20, 19),
(41, 5, 86, 84, 20, 19),
(42, 5, 13, 69, 6, 39),
(43, 5, 50, 70, 13, 3),
(44, 5, 60, 83, 13, 3),
(45, 5, 38, 73, 13, 3),
(46, 5, 25, 77, 13, 3),
(47, 5, 69, 78, 13, 3),
(48, 5, 81, 84, 13, 3),
(49, 5, 101, 75, 1, 43),
(50, 5, 113, 82, 1, 43),
(51, 5, 77, 78, 1, 43),
(52, 5, 89, 84, 1, 43),
(53, 5, 158, 72, 25, 43),
(54, 5, 165, 81, 25, 43),
(55, 5, 144, 76, 25, 43),
(56, 5, 134, 79, 25, 43),
(57, 5, 47, 73, 14, 43),
(58, 5, 56, 70, 153, 15),
(59, 5, 65, 83, 153, 15),
(60, 5, 43, 73, 153, 15),
(61, 5, 31, 77, 153, 15),
(62, 5, 122, 71, 153, 15),
(63, 5, 57, 70, 19, 15),
(64, 5, 66, 83, 19, 15),
(65, 5, 44, 73, 19, 15),
(66, 5, 32, 77, 19, 15),
(67, 5, 157, 72, 19, 15),
(68, 5, 164, 81, 19, 15),
(69, 5, 131, 79, 34, 15),
(70, 5, 132, 79, 29, 15),
(71, 5, 2, 67, 12, 21),
(72, 5, 8, 68, 12, 21),
(73, 5, 15, 69, 12, 21),
(74, 5, 20, 80, 12, 21),
(75, 5, 28, 77, 5, 21),
(76, 5, 153, 72, 9, 20),
(77, 5, 160, 81, 9, 20),
(78, 5, 139, 76, 9, 20),
(79, 5, 127, 79, 9, 20),
(80, 5, 61, 83, 1, 20),
(81, 5, 154, 72, 1, 20),
(82, 5, 161, 81, 1, 20),
(83, 5, 140, 76, 1, 20),
(84, 5, 128, 79, 1, 20),
(85, 5, 78, 78, 152, 24),
(86, 5, 90, 84, 152, 24),
(87, 5, 136, 79, 152, 24),
(88, 5, 35, 77, 26, 24),
(89, 5, 79, 78, 26, 24),
(90, 5, 91, 84, 26, 24),
(91, 5, 1, 67, 1, 7),
(92, 5, 7, 68, 1, 7),
(93, 5, 14, 69, 1, 7),
(94, 5, 19, 80, 1, 7),
(95, 5, 27, 77, 1, 7),
(96, 5, 40, 73, 1, 7),
(97, 5, 52, 70, 1, 7),
(98, 5, 48, 73, 15, 587),
(99, 5, 102, 75, 15, 587),
(100, 5, 114, 82, 15, 587),
(101, 5, 145, 76, 15, 587),
(102, 5, 146, 76, 24, 587),
(103, 5, 147, 76, 5, 587),
(104, 5, 103, 75, 22, 587),
(105, 5, 115, 82, 22, 587),
(106, 5, 16, 69, 3, 42),
(107, 5, 22, 80, 3, 42),
(108, 5, 41, 73, 8, 42),
(109, 5, 29, 77, 8, 42),
(110, 5, 118, 71, 1, 10),
(111, 5, 51, 70, 9, 10),
(112, 5, 62, 83, 9, 10),
(113, 5, 119, 71, 9, 10),
(114, 5, 39, 73, 9, 10),
(115, 5, 94, 75, 9, 10),
(116, 5, 106, 82, 9, 10),
(117, 5, 26, 77, 9, 10),
(118, 5, 70, 78, 9, 10),
(119, 5, 82, 84, 9, 10),
(120, 5, 21, 80, 6, 40),
(121, 5, 120, 71, 8, 71),
(122, 5, 155, 72, 8, 71),
(123, 5, 162, 81, 8, 71),
(124, 5, 95, 75, 8, 71),
(125, 5, 107, 82, 8, 71),
(126, 5, 141, 76, 8, 71),
(127, 5, 71, 78, 8, 71),
(128, 5, 83, 84, 8, 71),
(129, 5, 129, 79, 8, 71),
(130, 5, 54, 70, 10, 71),
(131, 5, 63, 83, 10, 71),
(132, 5, 42, 73, 10, 71),
(133, 5, 30, 77, 10, 71),
(134, 5, 5, 67, 7, 2),
(135, 5, 166, 68, 7, 2),
(136, 5, 18, 69, 7, 2),
(137, 5, 24, 80, 7, 2),
(138, 5, 59, 70, 25, 2),
(139, 5, 68, 83, 25, 2),
(140, 5, 46, 73, 25, 2),
(141, 5, 34, 77, 25, 2),
(142, 5, 6, 67, 3, 2),
(143, 5, 12, 68, 3, 2),
(144, 5, 117, 71, 13, 26),
(145, 5, 93, 75, 13, 26),
(146, 5, 105, 82, 13, 26),
(147, 5, 138, 76, 13, 26),
(148, 5, 126, 79, 13, 26),
(149, 5, 152, 72, 13, 26),
(150, 5, 159, 81, 13, 26),
(151, 5, 4, 67, 2, 73),
(152, 5, 10, 68, 2, 73),
(153, 5, 17, 69, 2, 73),
(154, 5, 58, 70, 20, 73),
(155, 5, 67, 83, 20, 73),
(156, 5, 125, 71, 25, 73),
(157, 5, 99, 75, 25, 73),
(158, 5, 111, 82, 25, 73),
(159, 5, 75, 78, 25, 73),
(160, 5, 87, 84, 25, 73),
(161, 5, 100, 75, 153, 73),
(162, 5, 112, 82, 153, 73),
(163, 5, 76, 78, 153, 73),
(164, 5, 88, 84, 153, 73),
(165, 6, 256, 94, 159, 6),
(166, 6, 257, 94, 160, 6),
(167, 6, 296, 97, 159, 6),
(168, 6, 297, 97, 160, 6),
(169, 6, 270, 95, 160, 6),
(171, 6, 271, 95, 161, 6),
(173, 6, 298, 97, 161, 6),
(174, 6, 242, 93, 166, 6),
(176, 6, 307, 98, 13, 3),
(178, 6, 251, 94, 13, 3),
(179, 6, 198, 90, 13, 3),
(180, 6, 179, 88, 6, 3),
(181, 6, 211, 91, 13, 3),
(183, 6, 243, 93, 168, 3),
(184, 6, 299, 97, 169, 3),
(185, 6, 178, 87, 7, 13),
(186, 6, 184, 88, 7, 13),
(187, 6, 199, 90, 7, 13),
(188, 6, 308, 98, 7, 13),
(190, 6, 212, 91, 7, 13),
(192, 6, 266, 95, 7, 13),
(193, 6, 236, 93, 7, 13),
(194, 6, 336, 101, 7, 13),
(195, 6, 182, 88, 3, 13),
(196, 6, 188, 89, 3, 13),
(197, 6, 172, 86, 7, 2),
(198, 6, 190, 89, 7, 2),
(199, 6, 252, 94, 7, 2),
(201, 6, 328, 100, 7, 2),
(203, 6, 292, 97, 7, 2),
(204, 6, 344, 102, 7, 2),
(205, 6, 170, 86, 3, 2),
(206, 6, 176, 87, 3, 2),
(212, 6, 302, 98, 10, 68),
(214, 6, 206, 91, 10, 68),
(216, 6, 260, 95, 10, 68),
(218, 6, 322, 100, 10, 68),
(219, 6, 232, 93, 10, 68),
(220, 6, 288, 97, 10, 68),
(221, 6, 332, 101, 10, 68),
(222, 6, 340, 102, 10, 68),
(223, 6, 171, 86, 12, 21),
(224, 6, 177, 87, 12, 21),
(225, 6, 183, 88, 12, 21),
(226, 6, 189, 89, 12, 21),
(237, 6, 192, 90, 9, 594),
(238, 6, 245, 94, 9, 594),
(239, 6, 301, 98, 9, 594),
(284, 6, 255, 94, 158, 587),
(285, 6, 268, 95, 158, 587),
(287, 6, 295, 97, 158, 587),
(288, 6, 254, 94, 157, 587),
(290, 6, 269, 95, 159, 587),
(294, 6, 294, 97, 157, 587),
(314, 6, 323, 100, 8, 71),
(315, 6, 207, 91, 8, 71),
(317, 6, 261, 95, 8, 71),
(319, 6, 333, 101, 8, 71),
(320, 6, 341, 102, 8, 71),
(321, 6, 233, 93, 8, 71),
(322, 6, 289, 97, 8, 71),
(324, 6, 193, 90, 10, 71),
(325, 6, 173, 87, 6, 26),
(326, 6, 185, 89, 6, 26),
(330, 6, 335, 101, 13, 26),
(331, 6, 343, 102, 13, 26),
(350, 6, 368, 104, 166, 6),
(351, 6, 174, 87, 1, 6),
(352, 6, 191, 90, 1, 6),
(353, 6, 246, 94, 10, 68),
(354, 6, 348, 103, 10, 68),
(355, 6, 358, 104, 10, 68),
(356, 6, 214, 91, 167, 36),
(357, 6, 238, 93, 167, 36),
(358, 6, 364, 104, 167, 36),
(359, 6, 241, 93, 165, 36),
(360, 6, 367, 104, 165, 36),
(361, 6, 169, 86, 2, 19),
(362, 6, 175, 87, 2, 19),
(363, 6, 306, 98, 153, 19),
(364, 6, 197, 90, 153, 19),
(365, 6, 250, 94, 153, 19),
(366, 6, 326, 100, 153, 19),
(367, 6, 352, 103, 153, 19),
(368, 6, 210, 91, 153, 19),
(369, 6, 264, 95, 153, 19),
(370, 6, 305, 98, 20, 19),
(371, 6, 196, 90, 20, 19),
(372, 6, 249, 94, 20, 19),
(373, 6, 324, 100, 19, 19),
(374, 6, 350, 103, 19, 19),
(375, 6, 208, 91, 19, 19),
(376, 6, 262, 95, 19, 19),
(377, 6, 265, 95, 13, 3),
(380, 6, 369, 104, 168, 3),
(381, 6, 327, 100, 13, 3),
(382, 6, 304, 98, 19, 15),
(383, 6, 195, 90, 19, 15),
(384, 6, 248, 94, 19, 15),
(385, 6, 334, 101, 19, 15),
(386, 6, 342, 102, 19, 15),
(387, 6, 234, 93, 19, 15),
(388, 6, 360, 104, 19, 15),
(389, 6, 290, 97, 19, 15),
(390, 6, 325, 100, 20, 15),
(391, 6, 351, 103, 20, 15),
(392, 6, 209, 91, 20, 15),
(393, 6, 263, 95, 20, 15),
(394, 6, 329, 100, 25, 15),
(395, 6, 355, 103, 25, 15),
(396, 6, 213, 91, 25, 15),
(397, 6, 267, 95, 25, 15),
(398, 6, 300, 98, 1, 20),
(399, 6, 320, 100, 1, 20),
(400, 6, 346, 103, 1, 20),
(401, 6, 204, 91, 1, 20),
(402, 6, 258, 95, 1, 20),
(403, 6, 330, 101, 1, 20),
(404, 6, 338, 102, 1, 20),
(405, 6, 230, 93, 1, 20),
(406, 6, 356, 104, 1, 20),
(407, 6, 286, 97, 1, 20),
(408, 6, 201, 90, 167, 24),
(409, 6, 202, 90, 164, 24),
(410, 6, 216, 91, 166, 24),
(411, 6, 203, 90, 166, 24),
(412, 6, 215, 91, 164, 24),
(413, 6, 239, 93, 162, 24),
(414, 6, 240, 93, 163, 24),
(415, 6, 365, 104, 162, 24),
(416, 6, 366, 104, 163, 24),
(417, 6, 180, 88, 1, 587),
(418, 6, 186, 89, 1, 587),
(419, 6, 244, 94, 1, 10),
(420, 6, 321, 100, 9, 10),
(421, 6, 347, 103, 9, 10),
(422, 6, 205, 91, 9, 10),
(423, 6, 259, 95, 9, 10),
(424, 6, 331, 101, 9, 10),
(425, 6, 339, 102, 9, 10),
(426, 6, 231, 93, 9, 10),
(427, 6, 287, 97, 9, 10),
(428, 6, 357, 104, 9, 10),
(429, 6, 349, 103, 8, 71),
(431, 6, 247, 94, 8, 71),
(432, 6, 194, 90, 8, 71),
(433, 6, 359, 104, 8, 71),
(434, 6, 354, 103, 7, 2),
(435, 6, 362, 104, 7, 2),
(436, 6, 167, 86, 6, 26),
(437, 6, 353, 103, 13, 26),
(438, 6, 235, 93, 13, 26),
(439, 6, 361, 104, 13, 26),
(440, 6, 291, 97, 13, 26),
(441, 6, 181, 88, 2, 73),
(442, 6, 187, 89, 2, 73),
(443, 6, 309, 98, 25, 73),
(444, 6, 200, 90, 25, 73),
(445, 6, 253, 94, 25, 73),
(446, 6, 337, 101, 25, 73),
(447, 6, 345, 102, 25, 73),
(448, 6, 237, 93, 25, 73),
(449, 6, 363, 104, 25, 73),
(450, 6, 293, 97, 25, 73),
(452, 6, 168, 86, 1, 73),
(453, 6, 370, 86, 172, 19),
(454, 6, 371, 87, 172, 21),
(455, 6, 372, 88, 172, 73),
(456, 6, 373, 89, 172, 26),
(457, 6, 374, 97, 171, 6),
(458, 6, 376, 93, 170, 24),
(459, 6, 375, 104, 170, 73),
(460, 6, 385, 105, 7, 13),
(461, 6, 378, 105, 9, 594),
(462, 6, 384, 105, 13, 3),
(463, 6, 386, 105, 25, 21),
(464, 6, 377, 105, 1, 587),
(465, 6, 379, 105, 10, 71),
(466, 6, 381, 105, 19, 73),
(467, 6, 382, 105, 20, 73),
(468, 6, 383, 105, 153, 73),
(473, 6, 303, 98, 8, 73),
(474, 6, 380, 105, 8, 73);
