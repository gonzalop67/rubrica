<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Usuarios
            <small>Listado</small>
        </h1>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="box box-primary">
            <!-- Default box -->
            <div class="box-header with-border">
                <a id="new_user" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#nuevoUsuarioModal"><i class="fa fa-plus-circle"></i> Nuevo Usuario</a>

                <div class="box-tools">
                    <form id="form-search" action="usuarios/buscar_usuario.php" method="POST">
                        <div class="input-group input-group-md" style="width: 250px;">
                            <input type="text" name="search_pattern" id="search_pattern" class="form-control pull-right text-uppercase" placeholder="Buscar Usuario..." value="<?= isset($datos['search_pattern']) ? $datos['search_pattern'] : '' ?>">

                            <div class="input-group-btn">
                                <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12 table-responsive">
                        <div id="mensajes"></div>
                        <table id="t_usuarios" class="table table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Id</th>
                                    <th>Avatar</th>
                                    <th>Nombre</th>
                                    <th>Usuario</th>
                                    <th>Activo</th>
                                    <th>Perfiles</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                            <tbody id="tbody_usuarios">
                                <!-- Aquí van los registros recuperados desde la BD -->
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </section>
</div>

<?php require_once "modalNuevoUsuario.php" ?>
<?php require_once "modalEditarUsuario.php" ?>

<script>
    $(document).ready(function() {
        $.ajaxSetup({
            error: function(jqXHR, textStatus, errorThrown) {
                if (jqXHR.status === 0) {
                    alert('Not connect: Verify Network.');
                } else if (jqXHR.status == 404) {
                    alert('Requested page not found [404]');
                } else if (jqXHR.status == 500) {
                    alert('Internal Server Error [500].');
                } else if (textStatus === 'parsererror') {
                    alert('Requested JSON parse failed.');
                } else if (textStatus === 'timeout') {
                    alert('Time out error.');
                } else if (textStatus === 'abort') {
                    alert('Ajax request aborted.');
                } else {
                    alert('Uncaught Error: ' + jqXHR.responseText);
                }
            }
        });

        // Primero recupero todos los usuarios de la base de datos
        cargar_usuarios();
        cargar_perfiles();

        $("#form-search")[0].reset();

        $("#form_insert")[0].reset();

        $("#new_us_foto").change(function() {
            $("#new_img_div").removeClass("hide");
            filePreview(this);
        });

        $("#edit_us_foto").change(function() {
            $("#new_img_div").removeClass("hide");
            filePreview(this);
        });

        $("#new_us_titulo").blur(function() {
            let us_titulo = $(this).val();
            let us_apellidos = $("#new_us_apellidos").val();
            let us_nombres = $("#new_us_nombres").val();

            let vec_us_apellidos = us_apellidos.split(" ");
            let vec_us_nombres = us_nombres.split(" ");
            $("#new_us_shortname").val(us_titulo + " " + vec_us_nombres[0] + " " + vec_us_apellidos[0]);
        });

        $("#new_us_apellidos").blur(function() {
            let us_titulo = $("#new_us_titulo").val();
            let us_apellidos = $(this).val();
            let us_nombres = $("#new_us_nombres").val();

            let vec_us_apellidos = us_apellidos.split(" ");
            let vec_us_nombres = us_nombres.split(" ");
            $("#new_us_shortname").val(us_titulo + " " + vec_us_nombres[0] + " " + vec_us_apellidos[0]);
        });

        $("#new_us_nombres").blur(function() {
            let us_titulo = $("#new_us_titulo").val();
            let us_apellidos = $("#new_us_apellidos").val();
            let us_nombres = $(this).val();

            let vec_us_apellidos = us_apellidos.split(" ");
            let vec_us_nombres = us_nombres.split(" ");
            $("#new_us_shortname").val(us_titulo + " " + vec_us_nombres[0] + " " + vec_us_apellidos[0]);
        });
    });

    function cargar_usuarios() {
        $.ajax({
            type: "get",
            url: "usuarios/cargar_usuarios.php",
            rType: "html",
            success: function(response) {
                $("#tbody_usuarios").html(response);
            }
        });
    }

    function cargar_perfiles() {
        $.ajax({
            type: "get",
            url: "usuarios/cargar_perfiles.php",
            dataType: "html",
            success: function(response) {
                $("#new_id_perfil").append(response);
                $("#edit_id_perfil").append(response);
            }
        });
    }

    function filePreview(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.readAsDataURL(input.files[0]);
            reader.onload = function(e) {
                $("#new_us_avatar").attr("src", e.target.result);
                $("#edit_us_avatar").attr("src", e.target.result);
            }
        }
    }

    $("#form-search").submit(function(e) {
        e.preventDefault();

        const search_pattern = $("#search_pattern").val().trim();
        const url = $(this).attr("action");

        $.ajax({
            url: url,
            method: 'POST',
            data: {
                search_pattern: search_pattern
            },
            dataType: "html",
            success: function(r) {
                $("#tbody_usuarios").html(r);
            }
        });
    });

    $("#form_insert").submit(function(e) {

        e.preventDefault();

        const us_titulo = $("#new_us_titulo").val().trim();
        const us_titulo_descripcion = $("#new_us_titulo_descripcion").val().trim();
        const us_apellidos = $("#new_us_apellidos").val().trim();
        const us_nombres = $("#new_us_nombres").val().trim();
        const us_shortname = $("#new_us_shortname").val().trim();
        const us_login = $("#new_us_login").val().trim();
        const us_password = $("#new_us_password").val().trim();

        let cont_errores = 0;

        if ($("#new_id_perfil").val().length == 0) {
            $("#mensaje10").html("Debe seleccionar al menos un perfil.");
            $("#mensaje10").fadeIn();
            cont_errores++;
        } else {
            $("#mensaje10").fadeOut();
        }

        if (us_titulo === '') {
            $("#mensaje1").html("Debe ingresar la abreviatura del Título.");
            $("#mensaje1").fadeIn();
            cont_errores++;
        } else {
            $("#mensaje1").fadeOut();
        }

        if (us_titulo_descripcion === '') {
            $("#mensaje2").html("Debe ingresar la descripción del Título.");
            $("#mensaje2").fadeIn();
            cont_errores++;
        } else {
            $("#mensaje2").fadeOut();
        }

        if (us_apellidos === '') {
            $("#mensaje3").html("Debe ingresar los Apellidos del Usuario.");
            $("#mensaje3").fadeIn();
            cont_errores++;
        } else {
            $("#mensaje3").fadeOut();
        }

        if (us_nombres === '') {
            $("#mensaje4").html("Debe ingresar los Nombres del Usuario.");
            $("#mensaje4").fadeIn();
            cont_errores++;
        } else {
            $("#mensaje4").fadeOut();
        }

        if (us_shortname === '') {
            $("#mensaje5").html("Debe ingresar el nombre corto del Usuario.");
            $("#mensaje5").fadeIn();
            cont_errores++;
        } else {
            $("#mensaje5").fadeOut();
        }

        if (us_login === '') {
            $("#mensaje6").html("Debe ingresar el nombre de usuario.");
            $("#mensaje6").fadeIn();
            cont_errores++;
        } else {
            $("#mensaje6").fadeOut();
        }

        if (us_password === '') {
            $("#mensaje7").html("Debe ingresar el password del usuario.");
            $("#mensaje7").fadeIn();
            cont_errores++;
        } else {
            $("#mensaje7").fadeOut();
        }

        if ($("#new_us_genero").val() === '') {
            $("#mensaje8").html("Debe seleccionar el género del usuario.");
            $("#mensaje8").fadeIn();
            cont_errores++;
        } else {
            $("#mensaje8").fadeOut();
        }

        if ($("#new_us_activo").val() === '') {
            $("#mensaje9").html("Debe especificar si está activo el usuario.");
            $("#mensaje9").fadeIn();
            cont_errores++;
        } else {
            $("#mensaje9").fadeOut();
        }

        var img = document.forms['form_insert']['new_us_foto'];
        var validExt = ["jpeg", "png", "jpg", "JPEG", "JPG", "PNG"];

        if (img.value != '') {
            var img_ext = img.value.substring(img.value.lastIndexOf('.') + 1);
            var result = validExt.includes(img_ext);

            if (result == false) {
                $("#mensaje11").html("Debe cargar un archivo de imagen.");
                $("#mensaje11").fadeIn();
                cont_errores++;
            } else {
                $("#mensaje11").fadeOut();
            }

            var CurrentFileSize = parseFloat(img.files[0].size / (1024 * 1024));

            if (CurrentFileSize >= 1) {
                $("#mensaje11").html("El archivo de imagen debe tener un tamaño máximo de 1 Mb. Tamaño actual: " + CurrentFileSize.toPrecision(4) + " Mb.");
                $("#mensaje11").fadeIn();
                cont_errores++;
            } else {
                $("#mensaje11").fadeOut();
            }
        } else {
            $("#mensaje11").html("Debe cargar un archivo de imagen.");
            $("#mensaje11").fadeIn();
            cont_errores++;
        }

        if (cont_errores == 0) {
            // submit el formulario
            var data = new FormData($("#form_insert")[0]);

            $.ajax({
                url: "usuarios/insertar_usuario.php",
                method: 'POST',
                data: data,
                contentType: false,
                processData: false,
                dataType: "json",
                success: function(response) {
                    cargar_usuarios();
                    Swal.fire({
                        title: response.titulo,
                        text: response.mensaje,
                        icon: response.tipo_mensaje,
                        confirmButtonText: 'Aceptar'
                    });
                    $('#form_insert')[0].reset();
                    $('#nuevoUsuarioModal').modal('hide');
                }
            });
        }
    });

    $("#form_update").submit(function(e) {

        e.preventDefault();

        const id_usuario = $("#edit_id_usuario").val();
        const us_titulo = $("#edit_us_titulo").val().trim();
        const us_titulo_descripcion = $("#edit_us_titulo_descripcion").val().trim();
        const us_apellidos = $("#edit_us_apellidos").val().trim();
        const us_nombres = $("#edit_us_nombres").val().trim();
        const us_shortname = $("#edit_us_shortname").val().trim();
        const us_login = $("#edit_us_login").val().trim();
        const us_password = $("#edit_us_password").val().trim();

        let cont_errores = 0;

        if ($("#edit_id_perfil").val().length == 0) {
            $("#mensaje21").html("Debe seleccionar al menos un perfil.");
            $("#mensaje21").fadeIn();
            cont_errores++;
        } else {
            $("#mensaje21").fadeOut();
        }

        if (us_titulo === '') {
            $("#mensaje12").html("Debe ingresar la abreviatura del Título.");
            $("#mensaje12").fadeIn();
            cont_errores++;
        } else {
            $("#mensaje12").fadeOut();
        }

        if (us_titulo_descripcion === '') {
            $("#mensaje13").html("Debe ingresar la descripción del Título.");
            $("#mensaje13").fadeIn();
            cont_errores++;
        } else {
            $("#mensaje13").fadeOut();
        }

        if (us_apellidos === '') {
            $("#mensaje14").html("Debe ingresar los Apellidos del Usuario.");
            $("#mensaje14").fadeIn();
            cont_errores++;
        } else {
            $("#mensaje14").fadeOut();
        }

        if (us_nombres === '') {
            $("#mensaje15").html("Debe ingresar los Nombres del Usuario.");
            $("#mensaje15").fadeIn();
            cont_errores++;
        } else {
            $("#mensaje15").fadeOut();
        }

        if (us_shortname === '') {
            $("#mensaje16").html("Debe ingresar el nombre corto del Usuario.");
            $("#mensaje16").fadeIn();
            cont_errores++;
        } else {
            $("#mensaje16").fadeOut();
        }

        if (us_login === '') {
            $("#mensaje17").html("Debe ingresar el nombre de usuario.");
            $("#mensaje17").fadeIn();
            cont_errores++;
        } else {
            $("#mensaje17").fadeOut();
        }

        if (us_password === '') {
            $("#mensaje18").html("Debe ingresar el password del usuario.");
            $("#mensaje18").fadeIn();
            cont_errores++;
        } else {
            $("#mensaje18").fadeOut();
        }

        if ($("#new_us_genero").val() === '') {
            $("#mensaje19").html("Debe seleccionar el género del usuario.");
            $("#mensaje19").fadeIn();
            cont_errores++;
        } else {
            $("#mensaje19").fadeOut();
        }

        if ($("#new_us_activo").val() === '') {
            $("#mensaje20").html("Debe especificar si está activo el usuario.");
            $("#mensaje20").fadeIn();
            cont_errores++;
        } else {
            $("#mensaje20").fadeOut();
        }

        var img = document.forms['form_insert']['new_us_foto'];
        var validExt = ["jpeg", "png", "jpg", "JPEG", "JPG", "PNG"];

        if (img.value != '') {
            var img_ext = img.value.substring(img.value.lastIndexOf('.') + 1);
            var result = validExt.includes(img_ext);

            if (result == false) {
                $("#mensaje22").html("Debe cargar un archivo de imagen.");
                $("#mensaje22").fadeIn();
                cont_errores++;
            } else {
                $("#mensaje22").fadeOut();
            }

            var CurrentFileSize = parseFloat(img.files[0].size / (1024 * 1024));

            if (CurrentFileSize >= 1) {
                $("#mensaje22").html("El archivo de imagen debe tener un tamaño máximo de 1 Mb. Tamaño actual: " + CurrentFileSize.toPrecision(4) + " Mb.");
                $("#mensaje22").fadeIn();
                cont_errores++;
            } else {
                $("#mensaje22").fadeOut();
            }
        }

        if (cont_errores == 0) {
            // submit el formulario
            var data = new FormData($("#form_update")[0]);

            $.ajax({
                url: "usuarios/actualizar_usuario.php",
                method: 'POST',
                data: data,
                contentType: false,
                processData: false,
                dataType: "json",
                success: function(response) {
                    cargar_usuarios();
                    Swal.fire({
                        title: response.titulo,
                        text: response.mensaje,
                        icon: response.tipo_mensaje,
                        confirmButtonText: 'Aceptar'
                    });
                    $('#form_update')[0].reset();
                    $('#editarUsuarioModal').modal('hide');
                }
            });
        }
    });

    function setearIndice(nombreCombo, indice) {
        for (var i = 0; i < document.getElementById(nombreCombo).options.length; i++)
            if (document.getElementById(nombreCombo).options[i].value == indice) {
                document.getElementById(nombreCombo).options[i].selected = indice;
            }
    }

    function obtenerDatos(id) {
        $.ajax({
            url: "usuarios/obtener_usuario.php",
            type: "POST",
            data: "id_usuario=" + id,
            dataType: "json",
            success: function(r) {
                // console.log(r);
                $("#edit_id_usuario").val(r.id_usuario);
                $("#edit_us_titulo").val(r.us_titulo);
                $("#edit_us_titulo_descripcion").val(r.us_titulo_descripcion);
                $("#edit_us_apellidos").val(r.us_apellidos);
                $("#edit_us_nombres").val(r.us_nombres);
                $("#edit_us_shortname").val(r.us_shortname);
                $("#edit_us_login").val(r.us_login);
                $("#edit_us_password").val(r.us_password);
                setearIndice('edit_us_genero', r.us_genero);
                setearIndice('edit_us_activo', r.us_activo);
                // edit_us_avatar
                $('#edit_us_avatar').attr('src', 'public/uploads/' + r.us_foto);
                $("#bd_us_avatar").val(r.us_foto);
                // document.getElementById("edit_us_avatar").src= 'public/uploads/' + r.us_foto;
            }
        });

        // Recuperamos los perfiles del usuario
        $.ajax({
            type: "POST",
            url: "usuarios/obtener_perfiles_usuario.php",
            data: "id_usuario=" + id,
            dataType: "json",
            success: function(r) {
                edit_id_perfil = document.getElementById("edit_id_perfil");
                // Limpiar los perfiles seleccionados anteriormente
                for (let i = 0; i < edit_id_perfil.length; i++) {
                    edit_id_perfil.options[i].selected = '';
                }
                // Recuperar los perfiles desde la base de datos
                for (let i = 0; i < r.length; i++) {
                    const id_perfil_usuario = r[i];
                    for (let j = 0; j < edit_id_perfil.length; j++) {
                        const edit_perfil_usuario = edit_id_perfil[j];
                        if (edit_perfil_usuario.value === id_perfil_usuario) {
                            edit_id_perfil.options[j].selected = 'selected';
                        }
                    }
                }
            }
        });
    }

    function eliminarUsuario(id_usuario) {
        Swal.fire({
            title: "¿Está seguro que quiere eliminar el registro?",
            text: "No podrá recuperar el registro que va a ser eliminado!",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: "#d33",
            cancelButtonColor: "#3085d6",
            confirmButtonText: "Sí, elimínelo!",
            cancelButtonText: 'Cancelar',
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    method: "post",
                    url: "usuarios/eliminar_usuario.php",
                    data: {
                        id_usuario: id_usuario
                    },
                    dataType: "json",
                    success: function(data) {
                        cargar_usuarios();
                        Swal.fire({
                            title: data.titulo,
                            text: data.mensaje,
                            icon: data.estado,
                            confirmButtonText: 'Aceptar'
                        });
                    },
                    error: function(xhr, ajaxOptions, thrownError) {
                        alert(xhr.status + "\n" + xhr.responseText + "\n" + thrownError);
                    }
                });
            }
        });
    }
</script>