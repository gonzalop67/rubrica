<?php
session_start();
// Conexión con la base de datos
require_once("scripts/clases/class.mysql.php");
require_once("scripts/clases/class.periodos_lectivos.php");
require_once("scripts/clases/class.encrypter.php");
if (!isset($_SESSION['usuario_logueado']))
	header("Location: index.php");
else {
	// Recepción de las variables GET
	$id_usuario = encrypter::decrypt($_GET['id_usuario']);
	$id_perfil = $_GET["id_perfil"];
	$db = new mysql();
	//Obtengo los nombres del usuario
	$consulta = $db->consulta("SELECT SUBSTRING_INDEX(us_apellidos, ' ', 1) AS primer_apellido, 
									  SUBSTRING_INDEX(us_nombres, ' ', 1) AS primer_nombre,
									  us_foto 
								 FROM sw_usuario 
								WHERE id_usuario = $id_usuario");
	$usuario = $db->fetch_assoc($consulta);
	$nombreUsuario = $usuario["primer_nombre"] . " " . $usuario["primer_apellido"];
	$userImage = "public/uploads/" . $usuario["us_foto"];
	if (!isset($_GET['nivel'])) {
		$titulo = "Dashboard";
		$enlace = "dashboard.php";
	} else {
		if (isset($_GET["enlace"])) {
			$enlace = $_GET["enlace"];
			$titulo = "Admin";
		} else {
			$consulta = $db->consulta("SELECT mnu_texto, 
											  mnu_enlace, 
											  mnu_nivel 
										 FROM sw_menu 
										WHERE mnu_publicado = 1 
										  AND id_menu = " . $_GET['id_menu']);
			$pagina = $db->fetch_assoc($consulta);
			$titulo = $pagina["mnu_texto"];
			$enlace = $pagina["mnu_enlace"];
			$nivel = $pagina["mnu_nivel"];
			$_SESSION['titulo_pagina'] = $titulo;
		}
	}
	$titulo = "SIAE Web | " . $titulo;
	//Obtengo el perfil del usuario logueado
	$consulta = $db->consulta("SELECT pe_nombre 
								 FROM sw_perfil 
								WHERE id_perfil = $id_perfil");
	$perfil = $db->fetch_assoc($consulta);
	$nombrePerfil = strtoupper($perfil["pe_nombre"]);
}
$id_periodo_lectivo = $_SESSION["id_periodo_lectivo"];
//Obtengo los años de inicio y de fin del periodo lectivo actual
$periodos_lectivos = new periodos_lectivos();
$periodo_lectivo = $periodos_lectivos->obtenerPeriodoLectivo($id_periodo_lectivo);
$meses_abrev = array(0, "Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic");
$fecha_inicial = explode("-", $periodo_lectivo->pe_fecha_inicio);
$fecha_final = explode("-", $periodo_lectivo->pe_fecha_fin);
$nombrePeriodoLectivo = $meses_abrev[(int)$fecha_inicial[1]] . " " . $fecha_inicial[0] . " - " . $meses_abrev[(int)$fecha_final[1]] . " " . $fecha_final[0];
//Obtengo el nombre de la modalidad asociada
$consulta = $db->consulta("SELECT mo_nombre FROM sw_modalidad m, sw_periodo_lectivo p WHERE m.id_modalidad = p.id_modalidad AND p.id_periodo_lectivo = $id_periodo_lectivo");
$modalidad = $db->fetch_object($consulta);
$nombreModalidad = $modalidad->mo_nombre;
?>
<!DOCTYPE html>
<html lang="es">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title><?php echo $titulo ?></title>
	<link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />
	<script src="js/keypress.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
	<!-- jquery-ui -->
	<link rel="stylesheet" href="assets/template/jquery-ui/jquery-ui.css">
	<link href="estilos.css" rel="stylesheet" type="text/css" />
	<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>-->
	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
	<!-- jquery-ui -->
	<script src="assets/template/jquery-ui/jquery-ui.js"></script>
	<!-- jquery-ui-validation -->
	<script src="public/js/jquery-validation/jquery.validate.min.js"></script>
	<script src="public/js/jquery-validation/localization/messages_es.min.js"></script>
	<!-- Chart JS -->
	<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
	<!-- plotly -->
	<script src="js/plotly-latest.min.js"></script>
	<!-- Theme style -->
	<link rel="stylesheet" href="assets/template/dist/css/AdminLTE.css">
	<!-- AdminLTE Skins. Choose a skin from the css/skins
    folder instead of downloading all of them to reduce the load. -->
	<link rel="stylesheet" href="assets/template/dist/css/skins/_all-skins.min.css">
	<!-- Font Awesome -->
	<link rel="stylesheet" href="assets/template/font-awesome/css/font-awesome.min.css">
	<!-- Ionicons -->
	<link rel="stylesheet" href="assets/template/Ionicons/css/ionicons.min.css">
	<!-- DataTables -->
	<link rel="stylesheet" href="assets/template/datatables.net-bs/css/dataTables.bootstrap.min.css">

	<!-- sweetalert 2 -->
    <link rel="stylesheet" href="assets/plugins/node_modules/sweetalert2/dist/sweetalert2.min.css">
	<script src="assets/plugins/node_modules/sweetalert2/dist/sweetalert2.all.min.js"></script>

	<!-- Select2 -->
	<script src="assets/template/select2/select2.min.js"></script>
	<link rel="stylesheet" href="assets/template/select2/select2.min.css">
	<!-- Toastr -->
	<link rel="stylesheet" href="assets/template/toastr/toastr.min.css">
	<script src="assets/template/toastr/toastr.min.js"></script>

	<script src="js/funciones.js"></script>

	<style>
		.error {
			color: #ff0000;
			display: none;
		}

		.rojo {
			color: #ff0000;
		}

		.taskDone {
			text-decoration: line-through;
		}

		.success {
			color: #006400;
			text-align: center;
			font-family: Arial, Helvetica, sans-serif;
		}

		.negrita {
			font-weight: bold;
		}

		.removeRow {
			background-color: #FF0000;
			color: #FFFFFF;
		}

		.chartjs {
			width: 310px;
			height: 310px;
			margin: 0 auto;
		}
	</style>

</head>

<body>
	<input type="hidden" id="id_usuario" value="<?php echo $id_usuario ?>">
	<input type="hidden" id="nombrePerfil" value="<?php echo $nombrePerfil ?>">
	<input type="hidden" id="id_periodo_lectivo" value="<?php echo $id_periodo_lectivo ?>">
	<input type="hidden" id="nombrePeriodoLectivo" value="<?php echo $nombrePeriodoLectivo ?>">
	<?php
	$menus = $db->consulta("SELECT * FROM sw_menu WHERE id_perfil = $id_perfil AND mnu_padre = 0 AND mnu_publicado = 1 ORDER BY mnu_orden");
	?>
	<nav class="navbar navbar-default">
		<div class="container-fluid">
			<div class="navbar-header">
				<a class="navbar-brand" href="admin2.php?id_usuario=<?php echo encrypter::encrypt($id_usuario) ?>&id_perfil=<?php echo $id_perfil ?>&enlace=dashboard.php&nivel=0">SIAE <?php echo $nombrePeriodoLectivo . "<br><small>" . $nombreModalidad . "</small>"; ?></a>
			</div>
			<ul class="nav navbar-nav">
				<?php
				while ($menu = $db->fetch_assoc($menus)) {
					$submenus = $db->consulta("SELECT * FROM sw_menu WHERE mnu_publicado = 1 AND mnu_padre = " . $menu['id_menu'] . " ORDER BY mnu_orden");
					$num_submenus = $db->num_rows($submenus);
					if ($num_submenus > 0) {
				?>
						<li class="dropdown">
							<a class="dropdown-toggle" data-toggle="dropdown" href="#">
								<?php echo $menu["mnu_texto"] ?> <span class="caret"></span>
							</a>
							<ul class="dropdown-menu">
								<?php
								while ($submenu = $db->fetch_assoc($submenus)) {
								?>
									<li>
										<a href="admin2.php?id_usuario=<?php echo encrypter::encrypt($id_usuario) ?>&id_perfil=<?php echo $id_perfil ?>&id_menu=<?php echo $submenu["id_menu"] ?>&nivel=<?php echo $submenu["mnu_nivel"] ?>">
											<?php echo $submenu["mnu_texto"] ?>
										</a>
									</li>
								<?php
								}
								?>
							</ul>
						</li>
					<?php
					} else {
					?>
						<li>
							<a href="admin2.php?id_usuario=<?php echo encrypter::encrypt($id_usuario) ?>&id_perfil=<?php echo $id_perfil ?>&id_menu=<?php echo $menu["id_menu"] ?>&nivel=<?php echo $menu["mnu_nivel"] ?>">
								<?php echo $menu["mnu_texto"] ?>
							</a>
						</li>
				<?php
					}
				}
				?>
			</ul>
			<?php
			if ($nombrePerfil == "TUTOR") {
				$paralelos = $db->consulta("
					SELECT p.id_paralelo, 
						   pa_nombre, 
						   cu_shortname  
					  FROM sw_paralelo_tutor pt, 
						   sw_paralelo p, 
						   sw_curso c
					 WHERE p.id_paralelo = pt.id_paralelo 
					   AND c.id_curso = p.id_curso
					   AND id_usuario = $id_usuario 
					   AND pt.id_periodo_lectivo = $id_periodo_lectivo");
				$num_registros = $db->num_rows($paralelos);
				$paralelo = $db->fetch_assoc($paralelos);
				$id_paralelo_tutor = $paralelo['id_paralelo'];
			}
			?>
			<input type="hidden" id="num_paralelos" value="<?php echo isset($num_registros) ? $num_registros : 1; ?>">
			<input type="hidden" id="id_paralelo_tutor" value="
					<?php
					if (isset($id_paralelo_tutor) && $_SESSION['cambio_paralelo'] == 0)
						echo $id_paralelo_tutor;
					else if (isset($_SESSION['id_paralelo_tutor']))
						echo $_SESSION['id_paralelo_tutor']
					?>">
			<div>
				<p class="navbar-text" id="nombre_paralelo">
					<?php
					if ($nombrePerfil == "TUTOR" && $_SESSION['cambio_paralelo'] == 0) {
						echo $paralelo['cu_shortname'] . " \"" . $paralelo['pa_nombre'] . "\"";
					} else if (isset($_SESSION['nombre_paralelo'])) {
						echo $_SESSION['nombre_paralelo'];
					}
					?>
				</p>
			</div>
			<ul class="nav navbar-nav navbar-right">
				<li>
					<?php
					$notificaciones = $db->consulta("SELECT COUNT(*) AS num_rows FROM sw_notificacion");
					$num_notificaciones = $db->fetch_object($notificaciones)->num_rows;
					$terminacion = $num_notificaciones == 1 ? 'ón' : 'ones';
					?>
					<a href="admin2.php?id_usuario=<?php echo $id_usuario ?>&id_perfil=<?php echo $id_perfil ?>&enlace=notificaciones/index.php&nivel=0" title="<?php echo $num_notificaciones . ' notificaci' . $terminacion; ?>">
						<i class="fa fa-bell-o"></i>
						<span class="label label-danger">
							<?php
							echo $num_notificaciones;
							?>
						</span>
					</a>
				</li>
				<li>
					<?php
					$comentarios = $db->consulta("SELECT COUNT(*) AS num_rows FROM sw_comentario WHERE id_usuario_para IN ($id_usuario, 0)");
					$num_comentarios = $db->fetch_object($comentarios)->num_rows;
					$terminacion = $num_comentarios == 1 ? '' : 's';
					?>
					<a id="link_num_comentarios" href="admin2.php?id_usuario=<?php echo $id_usuario ?>&id_perfil=<?php echo $id_perfil ?>&enlace=comentarios/index.php&nivel=0" title="<?php echo $num_comentarios . ' comentario' . $terminacion; ?>">
						<i class="fa fa-comments-o"></i>
						<span id="num_comentarios" class="label label-warning">
							<?php
							echo $num_comentarios;
							?>
						</span>
					</a>
				</li>
				<li class="dropdown">
					<a class="dropdown-toggle" data-toggle="dropdown" href="#">
						<!--<span class="glyphicon glyphicon-user"></span>--><img src="<?php echo $userImage; ?>" class="img-circle" alt="avatar image" width="30" /> <?php echo $nombreUsuario ?> <span class="caret"></span>
					</a>
					<ul class="dropdown-menu">
						<li><a href="admin2.php?id_usuario=<?php echo $id_usuario ?>&id_perfil=<?php echo $id_perfil ?>&enlace=change_password.php&nivel=0"><i class="fa fa-check-square-o" aria-hidden="true"></i> Cambiar Clave</a></li>
						<li><a href="admin2.php?id_usuario=<?php echo $id_usuario ?>&id_perfil=<?php echo $id_perfil ?>&enlace=comentarios/index.php&nivel=0"><i class="fa fa-comments-o" aria-hidden="true"></i> Comentarios</a></li>
						<?php
						if ($nombrePerfil == "TUTOR" && $num_registros > 1) {
						?>
							<li>
								<a href="admin2.php?id_usuario=<?php echo $id_usuario ?>&id_perfil=<?php echo $id_perfil ?>&enlace=tutores/view_cambiar_paralelo.php&nivel=0"><i class="fa fa-users" aria-hidden="true"></i> Cambiar Paralelo</a>
							</li>
						<?php
						}
						?>
						<li role="separator" class="divider"></li>
						<li><a href="logout.php"><i class="fa fa-sign-out" aria-hidden="true"></i> Salir</a></li>
					</ul>
				</li>
			</ul>
		</div>
	</nav>
	<div id="pagina_enlace">
		<?php include($enlace); ?>
	</div>
</body>

</html>