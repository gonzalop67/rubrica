<?php
	require_once("../scripts/clases/class.mysql.php");
	$db = new MySQL();
	$query = "
	SELECT * FROM `sw_usuario` 
	WHERE `us_activo` = 1
    AND `id_perfil` = 2  
	ORDER BY `us_apellidos`, `us_nombres`
	";
	$result = $db->consulta($query);
	$output = "";
	foreach($result as $row)
	{
		$id_usuario = $row["id_usuario"];
        $us_shortname = $row["us_shortname"];
		$output .= "<option value='$id_usuario'>";
        $output .= $us_shortname;
		$output .= "</option>";
    }

	echo $output;
?>