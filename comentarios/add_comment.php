<?php

//add_comment.php

require_once("../scripts/clases/class.mysql.php");
$db = new MySQL();

$error = '';
$comment_content = '';
$id_usuario = '';

$comment_content = $_POST["comment_content"];
$id_comentario_padre = $_POST["comment_id"];
$id_usuario = $_POST["id_usuario"];
$id_usuario_para = $_POST["id_usuario_para"];

// Obtengo el nombre completo del usuario
$query = "
SELECT us_shortname, 
       pe_nombre
FROM sw_usuario u, 
     sw_perfil p
WHERE p.id_perfil = u.id_perfil
  AND id_usuario = $id_usuario
";

$result = $db->consulta($query);
$usuario = $db->fetch_assoc($result);
$nombreCompleto = $usuario["us_shortname"];
$perfil = $usuario["pe_nombre"];

if($error == '')
{
    $query = "
    INSERT INTO sw_comentario 
    (id_comentario_padre, id_usuario_para, co_id_usuario, co_tipo, co_perfil, co_nombre, co_texto) 
    VALUES ($id_comentario_padre, $id_usuario_para, $id_usuario, 2, '$perfil', '$nombreCompleto', '$comment_content')
    ";

    $consulta = $db->consulta($query);
    if($consulta){
        $error = '<label class="text-success">Comentario A&ntilde;adido</label>';
    } else {
        $error .= '<p class="text-danger">No se pudo insertar el comentario...Error: '.mysqli_error($db->conexion).'<br />Consulta: '.$query.'</p>';
    }
    
}

$data = array(
    'error'  => $error
);

echo json_encode($data);

?>