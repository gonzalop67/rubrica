<?php
    include("clases/class.mysql.php");
	
	$db = new mysql();

	$id_periodo_evaluacion = $_POST['id_periodo_evaluacion'];
	$id_asignatura = $_POST['id_asignatura'];
	$id_paralelo = $_POST['id_paralelo'];
	
	$cadena = "";

	$consulta = $db->consulta("SELECT ap_abreviatura FROM sw_aporte_evaluacion WHERE id_periodo_evaluacion = $id_periodo_evaluacion");

	$num_total_registros = $db->num_rows($consulta);
    if($num_total_registros > 0) {
        //Cabecera de la tabla
        $cadena .= "<table class=\"table table-striped table-hover fuente9\">\n";
        $cadena .= "<thead>\n";
		$cadena .= "<tr>\n";
		$cadena .= "<th>Nro.</th>\n";
		$cadena .= "<th>Id</th>\n";
		$cadena .= "<th>N&oacute;mina</th>\n";

		$contador_aportes = 0;
		while($titulo_aporte = $db->fetch_assoc($consulta))
		{
			$contador_aportes++;
			if($contador_aportes < $num_total_registros)
				$cadena .= "<th>" . $titulo_aporte["ap_abreviatura"] . "</th>\n";
			else 
			{
				$cadena .= "<th>PROM.</th>\n";
				$cadena .= "<th>80%</th>\n";
				$cadena .= "<th>" . $titulo_aporte["ap_abreviatura"] . "</th>\n";
				$cadena .= "<th>20%</th>\n";
				$cadena .= "<th>NOTA Q.</th>\n";
			}
		}
		
		$cadena .= "</tr>\n";
		$cadena .= "</thead>\n";
		//Cuerpo de la tabla
		$cadena .= "<tbody>\n";
		$consulta = $db->consulta("SELECT e.id_estudiante, es_apellidos, es_nombres FROM sw_estudiante e, sw_estudiante_periodo_lectivo ep WHERE e.id_estudiante = ep.id_estudiante AND ep.id_paralelo = $id_paralelo AND es_retirado = 'N' AND activo = 1 ORDER BY es_apellidos, es_nombres ASC");
		$num_total_registros = $db->num_rows($consulta);
		if($num_total_registros > 0) {
			$contador = 0;
			while($estudiante = $db->fetch_assoc($consulta))
			{
				$contador++;
				$id_estudiante = $estudiante["id_estudiante"];
				$apellidos = $estudiante["es_apellidos"];
				$nombres = $estudiante["es_nombres"];
				$cadena .= "<tr>\n";
				$cadena .= "<td>$contador</td>\n";	
				$cadena .= "<td>$id_estudiante</td>\n";	
				$cadena .= "<td>".$apellidos." ".$nombres."</td>\n";
				//Aqui van las calificaciones de cada parcial
				$aportes_evaluacion = $db->consulta("SELECT id_aporte_evaluacion FROM sw_aporte_evaluacion WHERE id_periodo_evaluacion = $id_periodo_evaluacion");
				$num_total_registros = $db->num_rows($aportes_evaluacion);
				$suma_aportes = 0; $contador_aportes = 0; $suma_promedios = 0;
				while ($aporte_evaluacion = $db->fetch_assoc($aportes_evaluacion)) {
					$contador_aportes++;
					$id_aporte_evaluacion = $aporte_evaluacion['id_aporte_evaluacion'];
					
					$query = "SELECT id_rubrica_evaluacion 
								FROM sw_rubrica_evaluacion r,
									 sw_asignatura a
							   WHERE r.id_tipo_asignatura = a.id_tipo_asignatura
								 AND a.id_asignatura = $id_asignatura
								 AND id_aporte_evaluacion = $id_aporte_evaluacion";
					$rubrica_evaluacion = $db->consulta($query);
					$suma_rubricas = 0; $contador_rubricas = 0;
					while($rubricas = $db->fetch_assoc($rubrica_evaluacion))
					{
						$contador_rubricas++;
						$id_rubrica_evaluacion = $rubricas["id_rubrica_evaluacion"];
						$qry = $db->consulta("SELECT re_calificacion FROM sw_rubrica_estudiante WHERE id_estudiante = $id_estudiante AND id_paralelo = $id_paralelo AND id_asignatura = $id_asignatura AND id_rubrica_personalizada = $id_rubrica_evaluacion");
						$total_registros = $db->num_rows($qry);
						if($total_registros>0) {
							$rubrica_estudiante = $db->fetch_assoc($qry);
							$calificacion = $rubrica_estudiante["re_calificacion"];
						} else {
							$calificacion = 0;
						}
						$suma_rubricas += $calificacion;
					}
					$promedio = $suma_rubricas / $contador_rubricas;
					if($contador_aportes < $num_total_registros)
					{
						$suma_promedios += $promedio;
						$promedio = $promedio == 0 ? "" : substr($promedio, 0, strpos($promedio, '.') + 3);
						$cadena .= "<td>".$promedio."</td>\n";
					} else {
						$examen_quimestral = $promedio;
					}
				}
				$promedio_aportes = $suma_promedios / ($contador_aportes - 1);
				$ponderado_aportes = 0.8 * $promedio_aportes;
				$ponderado_examen = 0.2 * $examen_quimestral;
				$calificacion_quimestral = $ponderado_aportes + $ponderado_examen;

				$promedio_aportes = ($promedio_aportes == 0) ? "" : substr($promedio_aportes, 0, strpos($promedio_aportes, '.') + 3);
				$cadena .= "<td>".$promedio_aportes."</td>";
				$ponderado_aportes = ($ponderado_aportes == 0) ? "" : substr($ponderado_aportes, 0, strpos($ponderado_aportes, '.') + 3);
				$cadena .= "<td>".$ponderado_aportes."</td>";
				$examen_quimestral = ($examen_quimestral == 0) ? "" : number_format($examen_quimestral,2);
				$cadena .= "<td>".$examen_quimestral."</td>";
				$ponderado_examen = ($ponderado_examen == 0) ? "" : substr($ponderado_examen, 0, strpos($ponderado_examen, '.') + 3);
				$cadena .= "<td>".$ponderado_examen."</td>";
				$calificacion_quimestral = ($calificacion_quimestral == 0) ? "" :substr($calificacion_quimestral, 0, strpos($calificacion_quimestral, '.') + 3);
				$cadena .= "<td>".$calificacion_quimestral."</td>"; 
				
				$cadena .= "</tr>\n";
			}
		}
		$cadena .= "</tbody>\n";
		$cadena .= "</table>\n";
	}

	echo $cadena;
?>
