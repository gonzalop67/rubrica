<?php
/*
 * PHPExcel
 *
 * Copyright (C) 2006 - 2014 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2014 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    ##VERSION##, ##DATE##
 */

/* Error reporting */
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);

define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

date_default_timezone_set('America/Guayaquil');

function truncar($numero, $digitos)
{
    $truncar = pow(10,$digitos);
    return intval($numero * $truncar) / $truncar;
}

/* PHPExcel_IOFactory */

require_once '../php_excel/Classes/PHPExcel/IOFactory.php';
require_once '../scripts/clases/class.mysql.php';
require_once '../scripts/clases/class.paralelos.php';
require_once '../scripts/clases/class.institucion.php';
require_once '../scripts/clases/class.asignaturas.php';
require_once '../scripts/clases/class.periodos_lectivos.php';
require_once '../scripts/clases/class.periodos_evaluacion.php';

// Variables enviadas mediante POST
$id_paralelo = $_POST["id_paralelo"];
$id_periodo_evaluacion = $_POST["id_periodo_evaluacion"];

session_start();
$id_periodo_lectivo = $_SESSION["id_periodo_lectivo"];

$institucion = new institucion();
$nombreInstitucion = $institucion->obtenerNombreInstitucion();
$nombreRector = $institucion->obtenerNombreRector();
$nombreSecretario = $institucion->obtenerNombreSecretario();

$periodo_lectivo = new periodos_lectivos();
$nombrePeriodoLectivo = $periodo_lectivo->obtenerNombrePeriodoLectivo($id_periodo_lectivo);

$paralelo = new paralelos();
$id_curso = $paralelo->obtenerIdCurso($id_paralelo);
// $nombreParalelo = $paralelo->obtenerNombreParalelo($id_paralelo);
$db = new MySQL();
$consulta = $db->consulta("SELECT pa_nombre FROM sw_paralelo WHERE id_paralelo = $id_paralelo");
$record = $db->fetch_object($consulta);
$nombreParalelo = $record->pa_nombre;

$nomParalelo = $paralelo->getNombreParalelo($id_paralelo);

$periodo_evaluacion = new periodos_evaluacion();
$nombrePeriodoEvaluacion = $periodo_evaluacion->obtenerNombrePeriodoEvaluacion($id_periodo_evaluacion);

// Primero busco la plantilla adecuada de acuerdo al numero de asignaturas del paralelo
$numAsignaturas = $paralelo->contarAsignaturas($id_paralelo, $id_curso);
//if($tipoEducacion==0) $numAsignaturas++;

$objReader = PHPExcel_IOFactory::createReader('Excel5');
$baseFilename = "CUADRO QUIMESTRAL - ";
$objPHPExcel = $objReader->load("../templates/" . $baseFilename . $numAsignaturas . " ASIGNATURAS.xls");

$objPHPExcel->setActiveSheetIndex(0);
$objPHPExcel->getActiveSheet()->setCellValue('A1', $nombreInstitucion)
							  ->setCellValue('A3', 'CUADRO DE CALIFICACIONES - '.$nombrePeriodoEvaluacion)
							  ->setCellValue('B62', $nombreRector)
							  ->setCellValue('F62', $nombreSecretario);

// Renombrar la hoja de calculo
$objPHPExcel->getActiveSheet()->setTitle($nombrePeriodoEvaluacion);

$nombreCurso = $paralelo->getNombreCurso($id_paralelo); 

// Vectores de configuracion para las columnas
$colAsignaturas = array('C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S');

// Columna para escribir el promedio de las asignaturas
switch ($numAsignaturas) {
    case 6: 
		$colPromedio = 'I'; 
		$colNomPerLectivo = 'F'; 
		$colComportamiento = 'J'; 
		$colNomParalelo = 'I'; 
		break;
    case 7: 
		$colPromedio = 'J'; 
		$colNomPerLectivo = 'F'; 
		$colComportamiento = 'K'; 
		$colNomParalelo = 'J'; 
		break;
    case 8: 
		$colPromedio = 'K'; 
		$colNomPerLectivo = 'F'; 
		$colComportamiento = 'L'; 
		$colNomParalelo = 'K'; 
		break;
    case 9: 
		$colPromedio = 'L'; 
		$colNomPerLectivo = 'F'; 
		$colComportamiento = 'M'; 
		$colNomParalelo = 'L'; 
		break;
    case 10: 
		$colPromedio = 'M'; 
		$colNomPerLectivo = 'F'; 
		$colComportamiento = 'N'; 
		$colNomParalelo = 'M'; 
		break;
    case 11: 
		$colPromedio = 'N'; 
		$colNomPerLectivo = 'F'; 
		$colComportamiento = 'P'; 
		$colNomParalelo = 'N'; 
		break;
    case 12: 
		$colPromedio = 'O'; 
		$colNomPerLectivo = 'F'; 
		$colComportamiento = 'P'; 
		$colNomParalelo = 'O'; 
		break;
    case 13: 
		$colPromedio = 'P'; 
		$colNomPerLectivo = 'F'; 
		$colComportamiento = 'Q'; 
		$colNomParalelo = 'P'; 
		break;
    case 14: 
		$colPromedio = 'Q'; 
		$colNomPerLectivo = 'F'; 
		$colComportamiento = 'R'; 
		$colNomParalelo = 'Q'; 
		break;
    case 15: 
		$colPromedio = 'R'; 
		$colNomPerLectivo = 'I'; 
		$colComportamiento = 'S'; 
		$colNomParalelo = 'R'; 
		break;
    case 16: 
		$colPromedio = 'S'; 
		$colNomPerLectivo = 'I'; 
		$colComportamiento = 'T'; 
		$colNomParalelo = 'S'; 
		break;
    case 17: 
		$colPromedio = 'T'; 
		$colNomPerLectivo = 'F'; 
		$colComportamiento = 'V'; 
		$colNomParalelo = 'T'; 
		break;
}

$objPHPExcel->getActiveSheet()->setCellValue('A5', $nombreCurso);
$objPHPExcel->getActiveSheet()->setCellValue($colNomPerLectivo.'4', $nombrePeriodoLectivo);
$objPHPExcel->getActiveSheet()->setCellValue($colNomParalelo . '7', 'PARALELO "' . $nombreParalelo . '"');

$filaBase = 10; // fila base en la plantilla en Excel

// Aqui va el codigo para calcular el promedio del aporte de cada estudiante
// Se utilizara el store procedure sp_calcular_prom_quimestre que tiene los siguientes parametros:
//    IdPeriodoEvaluacion : $id_periodo_evaluacion (parametro POST)
//    IdParalelo : $id_paralelo (parametro POST)

// Primero se vaciará la tabla utilizada para almacenar los promedios del quimestre
$qry = "DELETE FROM sw_estudiante_prom_quimestral";
$res = $db->consulta($qry);
$qry = "CALL sp_calcular_prom_quimestre($id_periodo_evaluacion, $id_paralelo)";
$res = $db->consulta($qry);

//Seteo del Array para los promedios por Asignatura
$numero_calificaciones_validas[0] = 0;
$numero_calificaciones_validas[1] = 0;
$numero_calificaciones_validas[2] = 0;
$numero_calificaciones_validas[3] = 0;
$numero_calificaciones_validas[4] = 0;
$numero_calificaciones_validas[5] = 0;
$numero_calificaciones_validas[6] = 0;
$numero_calificaciones_validas[7] = 0;
$numero_calificaciones_validas[8] = 0;
$numero_calificaciones_validas[9] = 0;
$numero_calificaciones_validas[10] = 0;
$numero_calificaciones_validas[11] = 0;
$numero_calificaciones_validas[12] = 0;
$numero_calificaciones_validas[13] = 0;
$numero_calificaciones_validas[14] = 0;
$numero_calificaciones_validas[15] = 0;

$estudiantes = $db->consulta("SELECT e.id_estudiante, es_apellidos, es_nombres, eq_promedio FROM sw_estudiante e, sw_estudiante_prom_quimestral eq WHERE e.id_estudiante = eq.id_estudiante AND id_paralelo = $id_paralelo ORDER BY eq_promedio DESC");
$num_total_estudiantes = $db->num_rows($estudiantes);
if($num_total_estudiantes > 0)
{
	$row = $filaBase; // fila base
	$num_promedios_validos = 0;
	while($estudiante = $db->fetch_assoc($estudiantes))
	{
		$id_estudiante = $estudiante["id_estudiante"];
		$apellidos = $estudiante["es_apellidos"];
		$nombres = $estudiante["es_nombres"];
		$promedio_quimestral_total = $estudiante["eq_promedio"];

		$objPHPExcel->getActiveSheet()->setCellValue('B'.$row, $apellidos." ".$nombres);
		
		$asignaturas = $db->consulta("SELECT a.id_asignatura, a.id_tipo_asignatura, as_nombre FROM sw_asignatura_curso ac, sw_paralelo p, sw_asignatura a WHERE ac.id_curso = p.id_curso AND ac.id_asignatura = a.id_asignatura AND id_paralelo = $id_paralelo ORDER BY ac_orden");
		$total_asignaturas = $db->num_rows($asignaturas);
		if($total_asignaturas > 0)
		{
			$rowAsignatura = 8; 
			$contAsignatura = 0;
			$sumaComportamiento = 0;
			while ($asignatura = $db->fetch_assoc($asignaturas))
			{
				$num_calif_validas = 0;
				// Aqui proceso los promedios de cada asignatura
				$id_asignatura = $asignatura["id_asignatura"];
				$id_tipo_asignatura = $asignatura["id_tipo_asignatura"];
				$asignatura = $asignatura["as_nombre"];
				
				$objPHPExcel->getActiveSheet()->setCellValue($colAsignaturas[$contAsignatura].$rowAsignatura, $asignatura);
				
				if($id_tipo_asignatura==1) // Se trata de una asignatura CUANTITATIVA
				{
					// Aca voy a llamar a una funcion almacenada que calcula el promedio quimestral de la asignatura
					$query = $db->consulta("SELECT calcular_promedio_quimestre($id_periodo_evaluacion, $id_estudiante, $id_paralelo, $id_asignatura) AS promedio");
					$calificacion = $db->fetch_assoc($query);
					$promedio_quimestral = $calificacion["promedio"];

					if($promedio_quimestral > 0) $numero_calificaciones_validas[$contAsignatura]++;

					$objPHPExcel->getActiveSheet()->setCellValue($colAsignaturas[$contAsignatura].$row, truncar($promedio_quimestral,2));
				}

				$query = $db->consulta("SELECT calcular_comp_asignatura($id_periodo_evaluacion, $id_estudiante, $id_paralelo, $id_asignatura) AS comportamiento");
				$calificacion = $db->fetch_assoc($query);
				$comportamiento = $calificacion["comportamiento"];
				$sumaComportamiento += $comportamiento;

                $contAsignatura++;
			} // fin while $asignatura
			
			// Calculo e impresion del promedio de asignaturas
			$objPHPExcel->getActiveSheet()->setCellValue($colPromedio.$row, truncar($promedio_quimestral_total,2));

			if($promedio_quimestral_total > 0) $num_promedios_validos++;
			
			// Calculo e impresion del promedio de comportamiento
			$promedioComportamiento = $sumaComportamiento / $total_asignaturas;
			$promedio_comportamiento = ceil($promedioComportamiento);

			$query = $db->consulta("SELECT ec_equivalencia FROM sw_escala_comportamiento WHERE ec_correlativa = $promedio_comportamiento");
			$equivalencia = $db->fetch_assoc($query);
			$objPHPExcel->getActiveSheet()->setCellValue($colComportamiento.$row, $equivalencia['ec_equivalencia']);
		} // fin if $total_asignatura
		$row++;
	}
	// Elimino las filas excedentes
	if($num_total_estudiantes < 50)
		$objPHPExcel->getActiveSheet()->removeRow($row, $filaBase + 50 - $row);
	// Seteo las fórmulas para calcular los promedios generales de cada asignatura
	for($col = 0; $col < $contAsignatura; $col++){
		$objPHPExcel->getActiveSheet()->setCellValue($colAsignaturas[$col].$row, "=SUM(".$colAsignaturas[$col].$filaBase.":".$colAsignaturas[$col].($row-1).")/".$numero_calificaciones_validas[$col]);
	}
	//Aqui va la formula para el promedio de los promedios
	$objPHPExcel->getActiveSheet()->setCellValue($colAsignaturas[$col].$row, "=SUM(".$colAsignaturas[0].$row.":".$colAsignaturas[$col-1].$row.")/".$numAsignaturas);
}

// Aqui va el codigo para desplegar la lista de docentes del paralelo

$objPHPExcel->setActiveSheetIndex(1);
$docentes = $db->consulta("SELECT us_titulo, 
								  us_apellidos, 
								  us_nombres, 
								  as_nombre 
							 FROM sw_distributivo di,
							      sw_asignatura_curso ac, 
							 	  sw_usuario u, 
								  sw_asignatura a 
							WHERE u.id_usuario = di.id_usuario 
							  AND a.id_asignatura = di.id_asignatura
							  AND ac.id_asignatura = di.id_asignatura
							  AND ac.id_curso = $id_curso 
							  AND id_paralelo = $id_paralelo
							ORDER BY ac_orden");
$num_total_docentes = $db->num_rows($docentes);
if ($num_total_docentes > 0) {
	$row = 4;
	while ($docente = $db->fetch_object($docentes)) {
		$asignatura = $docente->as_nombre;
		$profesor = $docente->us_titulo . " " . $docente->us_apellidos . " " . $docente->us_nombres;
		$objPHPExcel->getActiveSheet()->setCellValue('B'.$row, $asignatura);
		$objPHPExcel->getActiveSheet()->setCellValue('D'.$row, $profesor);
		$row++;
	}
}

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save("CUADRO QUIMESTRAL "  . str_replace('"','',$nombreParalelo) . " " . $nombrePeriodoEvaluacion . "(" . $nombrePeriodoLectivo . ").xls");

// Codigo para abrir la caja de dialogo Abrir o Guardar Archivo

	header ("Expires: Mon, 26 Jul 1997 05:00:00 GMT");  
	header ("Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT");  
	header ("Cache-Control: no-cache, must-revalidate");  
	header ("Pragma: no-cache");  
	header ("Content-type: application/x-msexcel");
	header ("Content-Disposition: attachment; filename=\"" . "CUADRO QUIMESTRAL "  . str_replace('"','',$nombreParalelo) . " " . $nombrePeriodoEvaluacion . "(" . $nombrePeriodoLectivo . ").xls" . "\"" );
	readfile("CUADRO QUIMESTRAL "  . str_replace('"','',$nombreParalelo) . " " . $nombrePeriodoEvaluacion . "(" . $nombrePeriodoLectivo . ").xls");

?>