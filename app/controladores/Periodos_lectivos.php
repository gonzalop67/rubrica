<?php
class Periodos_lectivos extends Controlador
{
    private $modalidadModelo;
    private $periodoLectivoModelo;
    
    public function __construct()
    {
        session_start();
        if (!isset($_SESSION['usuario_logueado'])) {
            redireccionar('/auth');
        }
        $this->modalidadModelo = $this->modelo('Modalidad');
        $this->periodoLectivoModelo = $this->modelo('PeriodoLectivo');
    }

    public function index()
    {
        $periodos_lectivos = $this->periodoLectivoModelo->obtenerPeriodosLectivos();
        $datos = [
            'titulo' => 'Periodos Lectivos',
            'periodos_lectivos' => $periodos_lectivos,
            'nombreVista' => 'admin/periodos_lectivos/index.php'
        ];
        $this->vista('admin/index', $datos);
    }

    public function create()
    {
        $modalidades = $this->modalidadModelo->obtenerModalidades();
        $datos = [
            'titulo' => 'Crear Periodo Lectivo',
            'modalidades' => $modalidades,
            'nombreVista' => 'admin/periodos_lectivos/create.php'
        ];
        $this->vista('admin/index', $datos);
    }

    public function insert()
    {
        $datos = [
            'id_modalidad' => $_POST['id_modalidad'],
            'pe_anio_inicio' => trim($_POST['pe_anio_inicio']),
            'pe_anio_fin' => trim($_POST['pe_anio_fin']),
            'pe_fecha_inicio' => trim($_POST['pe_fecha_inicio']),
            'pe_fecha_fin' => trim($_POST['pe_fecha_fin'])
        ];

        try {
            $this->periodoLectivoModelo->insertarPeriodoLectivo($datos);
            $_SESSION['mensaje_exito'] = "Periodo Lectivo insertado exitosamente en la base de datos.";
            redireccionar('/periodos_lectivos');
        } catch (PDOException $ex) {
            $_SESSION['mensaje_error'] = "El Periodo Lectivo no fue insertado exitosamente. Error: " . $ex->getMessage();
            redireccionar('/periodos_lectivos');
        }
    }

    public function paginar()
    {
        $paginaActual = $_POST['partida'];
        $id_modalidad = $_POST['id_modalidad'];

        echo $this->periodoLectivoModelo->paginacion($paginaActual, $id_modalidad);
    }
}
?>