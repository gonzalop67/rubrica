<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Usuarios
        <small>Editar</small>
    </h1>
</section>
<!-- Main content -->
<section class="content">
    <!-- Default box -->
    <div class="box box-warning">
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel-body">
                        <div id="alert-error" class="alert alert-danger alert-dismissible" style="display:none">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <p><i class="icon fa fa-ban"></i> <span id="mensaje_error"></span></p>
                        </div>
                        <div id="alert-exito" class="alert alert-success alert-dismissible" style="display:none">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <p><i class="icon fa fa-check"></i> <span id="mensaje_exito"></span></p>
                        </div>
                        <form id="frm_usuario" class="form-horizontal" action="<?php echo RUTA_URL; ?>/usuarios/update" method="post" enctype="multipart/form-data">
                            <input type="hidden" name="id_usuario" id="id_usuario" value="<?php echo $datos['usuario']->id_usuario ?>">
                            <div class="form-group">
                                <label for="us_titulo" class="col-sm-2 control-label">Título:</label>
                                <div class="col-sm-10">
                                    <input type="text" name="us_titulo" id="us_titulo" value="<?php echo $datos['usuario']->us_titulo ?>" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="us_apellidos" class="col-sm-2 control-label">Apellidos:</label>
                                <div class="col-sm-10">
                                    <input type="text" name="us_apellidos" id="us_apellidos" value="<?php echo $datos['usuario']->us_apellidos ?>" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="us_nombres" class="col-sm-2 control-label">Nombres:</label>
                                <div class="col-sm-10">
                                    <input type="text" name="us_nombres" id="us_nombres" value="<?php echo $datos['usuario']->us_nombres ?>" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="us_login" class="col-sm-2 control-label">Usuario:</label>
                                <div class="col-sm-10">
                                    <input type="text" name="us_login" id="us_login" value="<?php echo $datos['usuario']->us_login ?>" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="us_password" class="col-sm-2 control-label">Password:</label>
                                <div class="col-sm-10">
                                    <?php $clave = encrypter::decrypt($datos['usuario']->us_password) ?>
                                    <input type="text" name="us_password" id="us_password" value="<?php echo $clave ?>" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="us_genero" class="col-sm-2 control-label">Género:</label>
                                <div class="col-sm-10">
                                    <select name="us_genero" id="us_genero" class="form-control">
                                        <option value="F" <?php echo $datos['usuario']->us_genero == 'F' ? 'selected' : '' ?>>Femenino</option>
                                        <option value="M" <?php echo $datos['usuario']->us_genero == 'M' ? 'selected' : '' ?>>Masculino</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="us_activo" class="col-sm-2 control-label">Activo:</label>
                                <div class="col-sm-10">
                                    <select name="us_activo" id="us_activo" class="form-control">
                                        <option value="1" <?php echo $datos['usuario']->us_activo == 1 ? 'selected' : '' ?>>Sí</option>
                                        <option value="0" <?php echo $datos['usuario']->us_activo == 0 ? 'selected' : '' ?>>No</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="id_perfil" class="col-sm-2 control-label">Perfil:</label>
                                <div class="col-sm-10">
                                    <select name="id_perfil[]" id="id_perfil" class="form-control" multiple size="7">
                                        <?php foreach ($datos['perfiles'] as $perfil) { 
                                            $selected = "";
                                            foreach ($datos['perfilesUsuario'] as $perfilUsuario) {
                                                if ($perfil->id_perfil == $perfilUsuario->id_perfil) {
                                                    $selected = "selected";
                                                    break;
                                                }
                                            }    
                                        ?>
                                            <option value="<?= $perfil->id_perfil; ?>" <?= $selected; ?>><?= $perfil->pe_nombre; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div id="img_upload">
                                <div class="form-group">
                                    <label for="us_avatar" class="col-sm-2 control-label"></label>

                                    <div id="img_div" class="col-sm-10">
                                        <?php $us_foto = $datos['usuario']->us_foto == '' ? 'no-disponible.png' : $datos['usuario']->us_foto ?>
                                        <img id="us_avatar" name="us_avatar" src="<?php echo RUTA_URL . '/public/uploads/' . $us_foto ?>" class="img-thumbnail" width="75" alt="Avatar del usuario">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="us_foto" class="col-sm-2 control-label" style="margin-top: -4px;">Imagen:</label>
                                    <input type="hidden" name="us_foto_file" id="us_foto_file" value="<?php echo $datos['usuario']->us_foto ?>">
                                    <div class="col-sm-10">
                                        <input type="file" name="us_foto" id="us_foto">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-2">
                                </div>
                                <div class="col-sm-10">
                                    <button id="btn-save" type="submit" class="btn btn-warning"><i class="fa fa-pencil"></i> Actualizar</button>
                                    <a href="<?php echo RUTA_URL; ?>/usuarios" class="btn btn-default"><i class="fa fa-backward"></i> Volver</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
</section>
<!-- /.content -->

<script>
    $(document).ready(function() {
        $("#us_foto").change(function() {
            $("#img_div").removeClass("hide");
            filePreview(this);
        });

        // $("#frm_usuario").on("submit", function(e) {
        //     e.preventDefault();

        //     let cont_errores = 0;
        //     const url = $(this).attr("action");

        //     // Validar los campos de entrada
        //     if ($("#id_perfil").val().length == 0) {
        //         swal("¡Error!", "Debe seleccionar al menos un perfil.", "error");
        //         cont_errores++;
        //     } else if ($("#us_titulo").val().trim() === '') {
        //         swal("¡Error!", "Debe ingresar el valor del campo Título.", "error");
        //         cont_errores++;
        //     } else if ($("#us_apellidos").val().trim() === '') {
        //         swal("¡Error!", "Debe ingresar el valor del campo Apellidos", "error");
        //         cont_errores++;
        //     } else if ($("#us_nombres").val().trim() === '') {
        //         swal("¡Error!", "Debe ingresar el valor del campo Nombres.", "error");
        //         cont_errores++;
        //     } else if ($("#us_login").val().trim() === '') {
        //         swal("¡Error!", "Debe ingresar el valor del campo Usuario.", "error");
        //         cont_errores++;
        //     } else if ($("#us_password").val().trim() === '') {
        //         swal("¡Error!", "Debe ingresar el valor del campo Password.", "error");
        //         cont_errores++;
        //     }

        //     var img = document.forms['frm_usuario']['us_foto'];
        //     var validExt = ["jpeg", "png", "jpg", "JPEG", "JPG", "PNG"];

        //     if (img.value != '') {
        //         var img_ext = img.value.substring(img.value.lastIndexOf('.') + 1);

        //         var result = validExt.includes(img_ext);

        //         if (result == false) {
        //             swal("¡Error!", "Debe cargar un archivo .jpg o .jpeg o .png", "error");
        //             cont_errores++;
        //             return false;
        //         } else {
        //             var CurrentFileSize = parseFloat(img.files[0].size / (1024 * 1024));
        //             if (CurrentFileSize >= 1) {
        //                 swal("¡Error!", "El archivo de imagen debe tener un tamaño máximo de 1 Mb. Tamaño actual: " + CurrentFileSize.toPrecision(4) + " Mb.", "error");
        //                 cont_errores++;
        //                 return false;
        //             }
        //         }
        //     }

        //     if (cont_errores == 0) {
        //         // submit el formulario
        //         var data = new FormData(this);

        //         $.ajax({
        //             url: url,
        //             method: "POST",
        //             data: data,
        //             contentType: false,
        //             cache: false,
        //             processData: false,
        //             dataType: "json",
        //             success: function(response) {
        //                 console.log(response);

        //                 /* swal({
        //                     title: response.titulo,
        //                     text: response.mensaje,
        //                     icon: response.tipo_mensaje,
        //                     confirmButtonTexto: 'Aceptar'
        //                 }); */

        //                 // $("#frm_usuario")[0].reset();
        //                 // $("#img_div").addClass("hide");
        //             },
        //             error: function(jqXHR, textStatus) {
        //                 console.log(jqXHR.responseText);
        //             }
        //         });
        //     }

        //     return true;
        // });
    });

    function filePreview(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.readAsDataURL(input.files[0]);
            reader.onload = function(e) {
                $("#us_avatar").attr("src", e.target.result);
            }
        }
    }
</script>