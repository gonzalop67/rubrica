<section class="content-header">
    <h1>
        Periodos Lectivos
        <small>Crear</small>
    </h1>
</section>
<!-- Main content -->
<section class="content">
    <!-- Default box -->
    <div class="box box-solid">
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-success">
                        <div id="titulo" class="panel-heading">Nuevo Periodo Lectivo</div>
                    </div>
                    <div class="panel-body">
                        <form id="frm-periodo-lectivo" action="<?= RUTA_URL . "/periodos_lectivos/insert" ?>" method="post">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="pe_anio_inicio">Año Inicial:</label>
                                        <input type="text" name="pe_anio_inicio" id="pe_anio_inicio" value="" class="form-control" autofocus>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="pe_anio_fin">Año Final:</label>
                                        <input type="text" name="pe_anio_fin" id="pe_anio_fin" value="" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div id="div_pe_fecha_inicio" class="form-group">
                                <label for="pe_fecha_inicio">Fecha de inicio:</label>
                                <div class="controls">
                                    <div class="input-group date">
                                        <input type="text" name="pe_fecha_inicio" id="pe_fecha_inicio" class="form-control">
                                        <label class="input-group-addon generic-btn" style="cursor: pointer;" onclick="$('#pe_fecha_inicio').focus();"><i class="fa fa-calendar" aria-hidden="true"></i></label>
                                        <span id="span_pe_fecha_inicio" class="help-block"></span>
                                    </div>
                                </div>
                            </div>
                            <div id="div_pe_fecha_fin" class="form-group">
                                <label for="pe_fecha_fin">Fecha de fin:</label>
                                <div class="controls">
                                    <div class="input-group date">
                                        <input type="text" name="pe_fecha_fin" id="pe_fecha_fin" class="form-control">
                                        <label class="input-group-addon generic-btn" style="cursor: pointer;" onclick="$('#pe_fecha_fin').focus();"><i class="fa fa-calendar" aria-hidden="true"></i></label>
                                        <span id="span_pe_fecha_fin" class="help-block"></span>
                                    </div>
                                </div>
                            </div>
                            <div id="div_id_modalidad" class="form-group">
                                <label for="id_modalidad">Modalidad:</label>
                                <select name="id_modalidad" id="id_modalidad" class="form-control">
                                    <option value="0">Seleccione...</option>
                                    <?php foreach($datos['modalidades'] as $v): ?>
                                        <option value="<?= $v->id_modalidad ?>">
                                            <?= $v->mo_nombre ?>
                                        </option>
                                    <?php endforeach; ?>
                                </select>
                                <span id="span_id_modalidad" class="help-block"></span>
                            </div>
                            <div class="form-group">
                                <button id="btn-save" type="submit" class="btn btn-success">Guardar</button>
                                <a href="<?php echo RUTA_URL . "/periodos_lectivos"; ?>" class="btn btn-default">Regresar</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
</section>
<!-- /.content -->

<script>
    $(document).ready(function(){
        $("#pe_fecha_inicio").datepicker({
            dateFormat : 'yy-mm-dd',
            firstDay: 1
        });

        $("#pe_fecha_fin").datepicker({
            dateFormat : 'yy-mm-dd',
            firstDay: 1
        });
    });
</script>
