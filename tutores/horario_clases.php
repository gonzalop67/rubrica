    <div class="content-wrapper">
        <div id="listaDocentesApp" class="col-sm-9 col-sm-offset-1">
            <h2>
                <div id="titulo">
                    <!-- Aqui va el titulo de la pagina -->
                </div>
            </h2>
            <!-- form -->
            <div class="panel panel-default">
                <form id="form_lista_docentes" action="php_excel/lista_docentes.php" class="app-form">
                    <input type="hidden" id="id_paralelo">
                    <!-- table -->
                    <table class="table table-bordered fuente9">
                        <thead id="horario_cabecera">
                            <!-- Aqui desplegamos los dias de la semana de este paralelo -->
                        </thead>
                        <tbody id="horario_clases">
                            <!-- Aqui desplegamos las horas clase con su asignatura y docente -->
                        </tbody>
                    </table>
                </form>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function() {
            $("#id_paralelo").val($("#id_paralelo_tutor").val());
            // Obtengo el nombre del curso para desplegar en el titulo
            $.ajax({
                url: "tutores/obtener_nombre_paralelo.php",
                data: {
                    id_paralelo: $("#id_paralelo_tutor").val()
                },
                method: "POST",
                type: "html",
                success: function(response){
                    $("#titulo").html("Horario de " + response);
                },
                error: function(xhr, status, error) {
                    alert(xhr.responseText);
                }
            });
            // Luego obtengo los dias de la semana
            $.ajax({
                url: "tutores/listar_dias_semana.php",
                data: {
                    id_paralelo: $("#id_paralelo_tutor").val()
                },
                method: "POST",
                type: "html",
                success: function(response){
                    $("#horario_cabecera").html(response);
                },
                error: function(xhr, status, error) {
                    alert(xhr.responseText);
                }
            });
            // Finalmente obtengo las horas clase con sus asignaturas y docentes
            $.ajax({
                url: "tutores/listar_horas_clase.php",
                data: {
                    id_paralelo: $("#id_paralelo_tutor").val()
                },
                method: "POST",
                type: "html",
                success: function(response){
                    $("#horario_clases").html(response);
                },
                error: function(xhr, status, error) {
                    alert(xhr.responseText);
                }
            }); 
        });
    </script>