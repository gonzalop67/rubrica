    <div class="content-wrapper">
        <div id="listaDocentesApp" class="col-sm-9 col-sm-offset-1">
            <h2>
                <div id="titulo">
                    <!-- Aqui va el titulo de la pagina -->
                </div>
            </h2>
            <!-- form -->
            <div class="panel panel-default">
                <form id="form_lista_docentes" action="php_excel/lista_docentes.php" class="app-form">
                    <input type="hidden" id="id_paralelo">
                    <!-- table -->
                    <table class="table fuente9">
                        <thead>
                            <tr>
                                <th>Nro.</th>
                                <th>Asignatura</th>
                                <th>Docente</th>
                            </tr>
                        </thead>
                        <tbody id="lista_docentes">
                            <!-- Aqui desplegamos el contenido de la base de datos -->
                        </tbody>
                    </table>
                    <!-- <button id="btn-submit" type="submit" class="btn btn-block btn-primary">
                        Reporte en Excel
                    </button> -->
                </form>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function() {

            $("#id_paralelo").val($("#id_paralelo_tutor").val());
            
            // Obtengo el nombre del curso para desplegar en el titulo
            $.ajax({
                url: "tutores/obtener_nombre_paralelo.php",
                data: {
                    id_paralelo: $("#id_paralelo_tutor").val()
                },
                method: "POST",
                type: "html",
                success: function(response){
                    $("#titulo").html("Lista de Docentes de " + response);
                },
                error: function(xhr, status, error) {
                    alert(xhr.responseText);
                }
            });
            
            // Luego obtengo la lista de docentes del paralelo
            $.ajax({
                url: "tutores/listar_docentes_paralelo.php",
                data: {
                    id_paralelo: $("#id_paralelo_tutor").val()
                },
                method: "POST",
                type: "html",
                success: function(response){
                    //console.log(response);
                    $("#lista_docentes").html(response);
                },
                error: function(xhr, status, error) {
                    alert(xhr.responseText);
                }
            });

        });
    </script>